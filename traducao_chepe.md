

Chepe Ruiz


Uma Viagem Ciclodélica
Uma ideia é o começo de tudo


Pequenos relatos




© Chepe Ruiz


Nos termos da Lei n° 9.610, de 19 de fevereiro de 1998, que regula os direitos autorais, é proibida a reprodução total ou parcial, bem como a produção de apostilas a partir desta obra, por qualquer forma, meio eletrônico ou mecânico, inclusive através de processos reprográficos, fotocópias ou gravações – sem permissão por escrito, dos Autores. A reprodução não autorizada, além das sanções civis (apreensão e indenização), está sujeita as penalidades que trata artigo 184 do Código Penal.




O lar é onde vive.
A felicidade é quando você ri.


Agradecimentos
Ruth Salazar (El Salvador), Adin Zetino (El Salvador), Aurelio Golsher (El Salvador), Simone Ariana (Italia), Marica van der Mer (Holanda), Leonardo Ramírez (El Salvador), Enio Paia (Brasil), Emilio Dergham (Argentina), Chato Pérez (El Salvador), Hugo Mollica (Uruguay) e Tânia Silva (Brasil).


A todas essas pessoas que sem me conhecer me ajudaram no caminho.
Chegaram como anjos no momento justo quando precisei de água, um lugar para dormir…
Quando necessitava alguém com quem falar, rir, para abraçar, para amar.
Os meus eternos heróis, OBRIGADO.


Prólogo


Então Chepe Ruiz chegou ao fim do mundo e, com a sua viagem, não fez mais que demonstrar que o fim do mundo é um oximoro, uma convenção que fixa um limite que não é como pensamos. Se o Ushuaia é o fim, que ponto geográfico é a origem? Por que o fim do mundo deveria estar na América do Sul? Se pelas terras sulistas se constitui o fim, o princípio haveria de estar no norte?
As rodas da bicicleta do Chepe Ruiz giram sem tempos nem espaços; escavando estatutos, jogando os preconceitos ao pó e, em seu movimento, lembrando que, em uma terra global, nessa aldeia circular que é nosso lar, não há princípio nem fim, não há norte nem sul se não decisões arbitrárias de latitudes que respondem a organizações sistemáticas que nos empurram goela abaixo.
Chepe Ruiz pedala e, na sua viagem, o vento fala e canta para ele. Mas, qual é a voz do vento? É grave? É aguda? Qual o seu timbre? Chepe descobre, a voz do vento não é singular, não é um cantor solista, a voz do vento é um coro, tem nuances, linhas, melodias alegres e tristes, eufóricas e temerosas. Entre suas vozes estão as dos moradores, que contam os deveres de uma sociedade de consumo. “Chepe”, lhe sussurram ao ouvido: “Com quanto dinheiro saiu para realizar sua viagem?, Tem seguro viagem?, Tem roupa suficiente para o frio patagônico?, Tem todas as peças extras da sua bicicleta para confrontar esses tantos de quilômetros despovoados?” A previsão e o calculo de nossas sociedades parametrizadas flutuam no ar do nosso inconsciente coletivo; nesse contexto, o vento penetra no coração do Chepe e, quando cai a tarde, o horizonte se mostra incerto e um princípio de desolação pesa na alma do viageiro gariteco2. Arrojado ao simples transcorrer não definido próprio da vida, em seu modo humilde aventureiro sem caminhos afrente, se pergunta: “Quanto da ordem cotidiana e suas prerrogativas utilitárias nos afastou da vida pela vida mesmo ao impor–nos tarefas de previsão e ordem?”.
Chepe se afasta das cidades e dos dizeres tipificados, daqueles que alguma vez, em um limite de fronteira, avisaram que somente com oito dólares não poderia entrar no Panamá.
Nas aforas, no recôndito, não lhe espera hotéis nem restaurantes, não possui um espaço alugado no qual passar a noite. No abrir de seu coração, sente essas outras vozes eólicas que lhe cantam as canções caladas, que dizem que até a sua condição de homem foi inventada e que, esta viagem, vem a mostrar–lhe que as diferenças que nos atribuímos para nos colocar sobre a natureza não são mais que a mentira mais bem contada. Chepe abraça a terra, arma sua barraca e se faz raiz em um solo latino–americano.
Este livro vem a contar–nos uma viagem da resistência, a resistência de um sonhador que, com suas ânsias do mundo e sem contar com os melhores recursos materiais, saiu de Garita Palmera em El Salvador e chegou até o extremo sul da Patagônia em uma simples bicicleta. Nos apresenta um périplo que é a própria vida e nos interpela a questionar os medos que nos são impostos em nossa cotidianidade. Quantos lugares, tormentas, alegrias, tristezas, pessoas, montanhas, campos, mares, comidas saíram ao encontro de forma imprevista nas andanças de nosso protagonista? É que o abraço a incertesa, o valor de soltar–se ao vazio e palpitar o movimento imprevisível que boia aberto, a alegria pela circunstância que se abre sem aviso é a forma de vida que mais se assemelha a vida.
Com a simpleza de um narrador sem voltas retóricas, mas com voltas do mundo, Chepe nos abre as janelas para essas vivências.

Emilio Ismael Dergham

A primeira pessoa que vi viajando em uma bicicleta foi quando tinha uns vinte e três anos. Esse dia voltava de Sonsonate, a maior cidade próxima de Garita Palmera, a prainha onde vivo.
Fui buscar uma segunda via do documento de identidade porque a primeira, devido a sua fragilidade e aos meus tipos de trabalhos, quebrou. Pensei que talvez as fabricavam com essa intenção.
Estava acostumado a viajar nos ônibus olhando pela janela. Vinha assim no momento em que o motorista parou o ônibus para subir uns passageiros. Percebi que do outro lado da estrada vinha um cara sem camiseta e com um grande chapéu pedalando uma bicicleta com grandes rodas e com malas por todos os lados. Certo que o pobre vinha assim porque o sol ardente esquentava muito aquela tarde.
Me chamou muito a atenção, logo o ônibus continuou e eu levantei para seguir vendo aquele louco estranho na bicicleta até que o perdi de vista na distância.
Os dias seguintes passei com essa imagem na minha cabeça até que por fim, o esqueci. Ou talvez não.
Uns poucos anos mais tarde, quando me dirigia a comprar uns materiais de construção em Cara Sucia, o povoado mais próximo, observei que haviam dois senhores tomando uns sucos fora de um pequeno armazém, na beira da estrada que leva para a fronteira com Guatemala.
Estavam ali, sentados em umas cadeiras de plástico descansando. Pareciam estranhos. Um deles se secava a testa suada com um lenço e o outro lia um mapa em um grande papel e com umas bicicletas carregadas de malas, encostadas em uma árvore do lado deles.
A poucos metros deles estava a casa de materiais de construção onde eu estava costumado a ir comprar frequentemente. Quando entrei, o lugar estava cheio de gente e eu tive que esperar um bom tempo para que me atendessem.
Nesses dias eu começava a construir minha casinha na praia pouco a pouco. Dessa vez comprei um pouco de cimento e ferro.
Assim que paguei o pedido dos materiais, saí da casa de materiais de construção e me dei conta que aqueles senhores que viajavam com bicicletas continuavam ali sentados.
Não aguentava mais de curiosidade e tentação que decidi ir conversar com eles.
– Oi senhores, boa tarde, sou Chepe.
Um deles se apresentou como Ray Ramírez e o outro me disse que se chamava Alberto Perz.
Me contaram que começaram a sua viagem desde a cidade de Puebla, no México e que saíram de suas casas nas suas bicicletas fazia pouco mais de um mês.
Perguntei para eles como era isso de sair a outros países montados numa bicicleta e de como deveria ser muito cansativa essa tarefa, do perigo que certamente corriam ao vir pelas estradas e das possibilidades de ser lançado por um motorista louco, apressado, bêbado ou pressionado pelo tempo ou por seu patrão. E não importar muito a eles machucá–los com seu trailer ou com seu carro e perder a vida assim.
O mais velho me respondeu que era verdade tudo isso do perigo. Que era trágico e difícil viajar assim de bicicleta mas que os momentos maravilhosos já vividos nesse pouco tempo que estavam fazendo isso, fazia que tudo valesse a pena. E que além de todo o mal que pudesse passar, somente com o fato de ter conseguido chegar a Cara Sucia pedalando, já era uma grande recompensa.
Fiquei muito surpreendido das suas convicções e de como eles compreendiam a vida.
Me veio a ideia de convidá–los para Garita Palmera e que acampassem na minha casa, porque me interessava os conhecer mais e seguir escutando–os.
– Será um prazer – disse Alberto que parecia mais jovem e o outro respondeu que também gostava da ideia.
Eles nas suas bicicletas e eu na minha Cabra (assim que chamava a minha bicicleta) saímos rumo a Garita, conversando; dizia para eles que me sentia muito feliz de ter pela primeira vez uns viajantes e que algum dia eu faria o mesmo que eles.
Estiveram dois ou três dias na minha casa, não lembro muito bem. Os levei para dar uma volta pelo __estero__ na minha canoa de madeira, até parei de trabalhar esses dias para andar com eles e aproveitar e desfrutar conversando de tudo um pouco. Fazendo perguntas para eles.
Ray me contou que era amigo do Alberto de quase toda a vida, desde muito pequenos. E que na fase da adolescência ficavam imaginando em fazer uma viajem de bicicleta por todo o mundo.
Mas enquanto o tempo ia passando, a ideia fantástica foi ficando guardada para depois. Cada um terminou seus estudos, buscaram trabalho, cada um encontrou a mulher de suas vidas e cada um deles se casou e constituíram um lar, família…
Entretanto, cada vez que podiam reunir as duas famílias, faziam isso.
E nessas ocasiões, já com os filhos grandes, contavam para eles aquela ideia que tinham na juventude, de viajar em bicicleta. Suas esposas também conheciam essa história de cor e salteado.
Bem, os anos continuaram passando e eles envelhecendo, até que reunidas as famílias completas num natal ou num aniversário, um dos filhos pediu para falar algo para todos e contou que eles, os filhos, estavam se reunindo e conversando sobre o assunto e que conseguiram chegar a um acordo em conjunto.
Então incentivaram seus queridos pais a realizar um dos maiores sonhos de suas vidas que era esse, o de ir pelo mundo nas suas bicicletas (ou pelo menos, onde eles quisessem) e que estavam todos os filhos de acordo em apoiar um pouco financeiramente.
No princípio disseram que não, logo que pensariam mas em pouco tempo eles foram gostando mais da ideia e lhes parecia boa e atrevida. Seria uma boa aventura e tentariam.
Assim que, ao final, planejaram viajar desde México até a Argentina em bicicleta. E decidiram chamar o projeto da viagem de “Cicloprojeto caracol” devido a vagareza e tranquilidade que queriam fazer ele.
A manhã que foram embora, lembro que nos despedimos com um forte abraço e com promessas de manter a amizade e a comunicação pelo menos por e–mail.
E assim o fizemos durante todo o tempo que durou sua viagem. E ainda, de vez em quando, nos escrevemos.
Desde então cada vez que viajava ao povoado na minha bicicleta e olhava os viajantes, trazia eles a minha casa compartindo com eles o que podia.
Um viajante deu meu endereço para outro que encontrou pelo caminho e assim, de boca em boca, foram chegando a minha casa vários e de todas partes do mundo.
No começo do ano de 2014, encontrei comendo em uma pupuseria3 outro que também viajava em bicicleta, a quem considero que merece ser mencionado nessa história, Chris Parkin.
Vinha de Inglaterra e não falava quase nada de espanhol. Depois de uma pequena conversa aceitou vir para minha casa e descansar uns dias.
Chris era vegetariano, gostava de andar por aí tirando fotos dos insetos e animais argumentando que eram muito bonitos e que mereciam admiração. Tando assim que ele não gostava nem matar os mosquitos que chupavam seu sangue enquanto meditava nas noites. Eu encontrava ele com suas varinhas de incensos aromáticos acendidos, sentado na posição de lótus e com as costas cheias de pernilongos e, no outro dia, amanhecia com um monte de picadas por todos lados do seu corpo.
Foi para ele que contei que na verdade estava considerando fazer minha primeira viagem em bicicleta, começando por percorrer América Central. Faça–o, me disse.
Logo soube as circunstâncias da vida que levaram Chris a realizar essa viagem e me surpreendi ao ver que muito coincidiam com as minhas. No caso dele, estava dando um resultado muito bom.
Não sei o que me deu, mas comecei a pôr em prática as rotinas de meditação do Chris Parkin, também acendia varinhas de incenso, mas a princípio sim matava os mosquitos que me picavam, logo já não foi necessário porque não sei nem como, deixaram de me incomodar.
Depois disso, começou a surgir em mim atitudes muito boas, aprendi a ter mais paciência e a levar as coisas com calma. Aí tudo começou a melhorar.
Fabiola, minha filha, que havia ido estudar longe, voltou e se mudou para a casa. Logo, não sei nem como, conhecí a uma maravilhosa mulher bem mais jovem que eu, que de pura sorte ou milagre do destino, decidiu ficar e dividir a vida comigo.
Com elas voltei a sentir em uma família e em pouco tempo começamos um pequeno projeto de reciclagem de garrafas plásticas e pneus de carros.
Saíamos a recolher as garrafas das ruas e fabricamos lixeiras e presenteávamos aos restaurantes, às lojas e para a escola.
Com os pneus de carros, floreiras para plantar flores, os pintávamos com bonitas cores e os presenteávamos aos amigos. Plantamos uma pequena horta com o que se podia cultivar na nossa zona tropical. O tentar realizar essas novas atividades criativas era muito entretido e interessante.
Já emocionados nessas coisas começamos a buscar outros meios de adquirir novos conhecimentos no tema da permacultura e na farmácia natural. Assim que, lá pelo fim desse ano, decidimos ir morar por um tempo numa ilha na Nicarágua. Uma decisão que não seria nada fácil mas que marcaria para sempre nossas vidas.
O lago Cocibolca é o maior lago em toda América Central, com uma extensão de quase a metade do território de El Salvador. E aí, rodeada por esse lindo lago, está a ilha de Ometepe.
Os primeiros dias que chegamos na ilha passamos desfrutando das praias com amigos que encontrávamos e outros que vinham visitar. Dormindo nas nossas barracas, um tempo por aqui, outros dias por ali, até que depois de uns meses conhecemos o seu Gilberto, um amigo nativo da ilha. Ele nos contou que perto de onde ele morava havia uma casinha para alugar. E fomos morar aí.
Matriculei minha filha na escola e em poucos dias começaram as aulas, tudo estava saindo bem.
Mas entre pagamentos de aluguel, materiais escolares e comida foi acabando o dinheiro. Logo me dei conta que na ilha não era tão fácil encontrar um bom trabalho. Decidi lutar por nossos projetos, comecei a fazer todo tipo de bico que saía ou que me avisavam. Desde capinar com um facão, picar lenha, carregador de areia e pedras num caminhão... Tudo que fosse com o propósito de conseguir levar algo de comida para casa. Assim, literalmente o fazia. Lembro que as vezes, com o que me pagavam em um bico apenas alcançava para comprar meio quilo de queijo e logo, com o que ganhava no dia seguinte, conseguia comprar meio quilo de açúcar e uma garrafa de óleo, etc... Assim fomos vivendo nosso início, nos conformando e com pouquinho.
Mas lá pela metade do ano já estávamos melhorando4. Começamos a conhecer mais pessoas, que apesar das suas carências, compartilhavam do que tinham. Traziam cachos de bananas, pacotes de feijão, boa lenha, muita fruta. Nunca passamos fome, sempre amanhecia algo de comer na nossa mesa. Logo encontrei um bom trabalho como soldador em um grande hotel. A mulher com quem vivia também trabalhava de vez em quando ajudando na faxina da casa de alguma vizinha. E por fim começamos a poupar um pouco.
No hotel onde trabalhava ficava uns doze quilômetros da nossa casa. Levantava–me bem cedo nas manhãs e saia a rua a fazer sinal aos carros para que me levassem e algumas vezes fui caminhando para economizar o pagamento da passagem de ônibus. Mas uma noite, voltando de trabalhar, percebi que atrás da casa do seu Gilberto, meu vizinho, estava jogada uma pequena bicicleta. Cheguei em casa e depois de jantar fui perguntar ao seu Gilberto sobre a bicicleta. Resulta que era de um dos seus filhos, mas que fazia um bom tempo que não a usava por ter uns problemas.
Na noite seguinte, meu bom vizinho chegou em casa trazendo consigo a pequena bicicleta.
– Falei com meu filho sobre o assunto – me disse – e está de acordo em te emprestar. O único, Chepe, que terá que colocar freios e consertar as rodas.
Com o que me pagaram pelo trabalho que fiz no hotel essa semana, levei a bicicleta à bicicletaria para que deixassem funcionando. E desde então, aquela pequena bicicleta se converteu no meu inseparável meio de transporte. Com ela percorria os vinte e quatro quilômetros, todos os dias, de ida e volta da casa onde vivíamos até o trabalho e assim o fiz por mais de um ano.
Uma noite, falando com minha filha do planejamento da sua festa de quinze anos me disse:
– Papai, melhor não gastemos em fazer uma grande festa. Façamos uma modesta e normal janta para a gente e algumas companheiras da escola e que venha minha avó nos visitar. Olha, o que eu mais queria depois de terminar meu ensino médio é ir fazer faculdade em algum país da América do Sul, pode ser na Argentina ou no Uruguai. Lí que nesses países as universidades são gratuitas e acessíveis para os estrangeiros. Eu almejo estudar pediatria papai.
Aquelas palavras ficariam muito marcadas na minha mente e até hoje em dia as lembro com clareza e com muito carinho.
Bom, o que passou nos messes seguintes vou te contando de pouco em pouco, em pequenos relatos, que fui escrevendo conforme os acontecimentos foram passando.
Decidi fazer assim para que não te pareça entediante ler esse livro, assim você escolhe a forma de ir descobrindo de pouquinho, relato por relato, cada manhã ou a tarde, acompanhado com uma boa xícara de café, um chimarrão quente ou com uma boa cerveja, porque não.
Como você goste. Meu método favorito de leitura é ler algumas páginas de um livro minutos antes de dormir.
Agora, só resta da minha parte te agradecer por ter adquirido esse simples livro e pela honra que sinto de que dediques um pouquinho do tempo da sua valiosa vida em lê–lo. E que ao fim, o presenteie a outra pessoa que você considera que possa interessar.
Chepe Ruiz
21–02–2020
Montevideo, Uruguai


#Setembro 2016
Passou o sábado.
Voltava na bicicleta depois de comprar alguns mantimentos do povoado de Altagracia.
Já longe e pela zona mais arborizada onde não há casas, foi que aconteceu.
Rapidamente todo o entorno mudou de supetão5 pois me senti em outro lugar, como em outro país; respirei outro cheiro, me vi sobre outra bicicleta. Inclusive me sentia que era outro eu.
Eu não sei, mas pode ser que aquilo me levou a um êxtase estranho e de repente me caguei de rir. E ria como um louco em liberdade. Senti uma grande alegria, felicidade; me senti livre e com muita paz. Foi tão fugaz como intenso e fascinante de uma vez.
Logo continuei pedalando até chegar em casa, com a experiência guardada no meu peito. Depois do almoço contei para elas.
Disse que me senti viajando de bicicleta por outros países e que era o que agora de verdade queria fazer.
Fabiola, minha filha, me disse que a ideia era bonita e maravilhosa, que o fizesse e que além isso se ajustava aos planos de seus estudos em outro país.
Hilda, entretanto, virou seu rosto para mim, devagar e me olhou, se levantou da cadeira e foi deitar na rede, em silêncio…


A almofadinha
No ano seguinte ficamos morando na ilha sozinhos, Hilda e eu. Fabiola quis seguir com seus estudos em Guatemala morando com minha mãe.
A gente se mudou a uma comunidade indígena onde teria, ao fim, a oportunidade de aprender algumas técnicas de permacultura.
– Eu quero aprender a andar de bicicleta – me disse Hilda uma manhã enquanto eu saia da fazenda até o trabalho.
Me olhou, levantou suas grandes e lindas pestanas com os olhos negros bem abertos disse:
– Me ensina a andar de bicicleta.
Desde então cada domingo, logo depois do café da manhã, íamos a um campinho onde as crianças jogavam bola pelas tardes. Abaixava um pouco o assento da bicicleta, ela subia, logo eu a segurava guiando com uma mão o guidão e a outra nas suas costas e começava a girar os pedais.
Se concentra, se concentra, não olhe a roda, olha para frente, não deixe de pedalar, mantêm o equilíbrio... e assim, domingo a domingo, até que aprendeu a andar sozinha.
Depois de umas poucas semanas, seu Gonzalo, um professor aposentado que se tornou nosso amigo, me presenteou uma bicicleta maior de montanha e que também tive que levar a bicicletaria para uns consertos.
Quando Hilda soube andar de bicicleta muito bem, começamos a ir todos os domingos juntos a todos lados, ela na sua bicicletinha e eu na grande.
Íamos a Altagracia para comprar os alimentos ou simplesmente a tomar um chocolate com leite gelado e a sentar em um banco do parque.
Logo começou a vir comigo ao hotel onde eu trabalhava porque me dizia que se entediava sozinha na casa. Me ajudava a pintar e preparava uma salada para o almoço.
Um sábado que recebi pagamento, de volta a casa passamos para almoçar em um bonito restaurante na beira do lago.
Depois de comer, fomos sentar em um gande tronco de árvore pertinho da água e ficamos ali contemplando toda a beleza ao nosso redor, em silêncio.
– Quero ir com você nessa viagem que vai fazer em bicicleta – disse.
– Tá falando sério? – perguntei.
– Sim – respondeu.
Bem, então temos que começar a planificar tudo desde agora, tem que praticar mais com a bicicleta como por exemplo, subir uma subida íngrime ou fazer trechos mais longos, a hidratação, alongar, puf! Muitas coisas mais.
– Bom, comecemos – disse.
E logo nos dois fomos para casa sentindo muito alegres. Falávamos todos os dias da viagem, de como seria, que teríamos que levar, a data de saída, etc.
Eu estava tão contente que Hilda, a menina das grandes pestanas, olhos bonitos e coração terno, queria vir viajar de bicicleta comigo.
Iniciamos uma nova rotina de pedalada cada domingo, cada vez chegávamos a um povoado mais longe, tratávamos de dar o máximo até sentir que não podíamos mais continuar.
Um dia, convidamos para vir com a gente o seu Evenor, um vizinho que era o alfaiate da nossa comunidade, de uns sessenta e três anos de idade, os três demos a volta na ilha percorrendo noventa e cinco quilômetros na estrada que a ródea.
Os trabalhos aumentaram e o dinheiro começou a deixar de ser um problema. Comprei uma boa bicicleta para ela em uma casa de penhor, considerando que aguentaria mais distâncias, o melhor possível para que ela viajasse cômoda; um fim de semana fomos a um grande povoado comprar as coisas mais indispensáveis para nosso projeto de viagem.
Uma panelinha para o café, uma frigideira para fritar os ovos, uma garrafa para a água, mochilas usadas em bom estado e todo aquilo que nos pudesse servir no caminho.
Já íamos saindo do mercado caminhando rumo ao porto, quando de repente vejo que ela entra em um brechó. Eu não sou muito bom para ir comprar e além disso, me sentia cansado, então fiquei por ali sentado, esperando debaixo de uma arvorezinha de guarumo6. Saiu com uma sacola preta de plástico pendurada na sua mão.
– Que comprou Hilda? – a perguntei.
– Olhe – me respondeu com um grande sorriso no seu rosto.
Peguei a sacola, a abri e tirei uma pequena almofadinha, de um lado verde suave e do outro lado uma cor preto mate e tinha desenhos de flores bordadas com lantejoulas douradas.
Não sei porque mas não aguentei a risada e ri na frente dela, de uma forma muito sarcástica e perguntei:
– Mas... e isso para qué? Se temos como quatro almofadas na casa e você decide gastar o dinheiro em outra…
Aquele belo sorriso desapareceu do seu rosto e várias rugas apareceram na sua testa.
– Comprei porque eu quero – disse –, porque para mim é mais prático para levar e muito necessária. Não posso ir viajar pelo mundo de bicicleta sem uma almofada para dormir bem.
Me arrancou das mãos, meteu na sacola, agarrou sua bicicleta e se foi sem falar comigo. Somente fui atrás dela, refletindo um pouco até que chegamos em casa. Entramos e comemos, mas antes de dormir pedi a ela que por favor me desculpasse.
Quando chegou no mês de dezembro sabíamos que eram os últimos dias para ir ver e compartir com nossas famílias.
Ela passaria o natal com seus pais e suas irmãs e eu fui para Guatemala passar com minha mãe e minha filha, Nos primeiros dias de janeiro voltamos a estar juntos em El Salvador, em nossa casa em Garita Palmera. Uma noite, enquanto voltávamos de uma pupusería, me disse:
– Já não vou com você. Estive pensando melhor, creio que essa viagem de bicicleta é sua viagem, seus planos, seu sonho. Não são os meus. Além disso, me dão muito medo os carros e os caminhões que passam muito perto. Creio que não vou me acostumar a um estilo de vida assim. Assim, como vivem os ciganos que andam de um lado para o outro. Me senti como desmoronando e muito surpreendido ao escutar suas palavras, sério e mais a essa altura que já tínhamos quase tudo pronto.
– Mas por quê? – perguntei –. Se são nossos planos.
– Não, Chepe, são seus planos eu quero algo mais que uma viagem.
– Bem e me conta quais são então seus planos – e aí começou a chorar…
– Um lar – me respondeu. Quero um lar normal com você, quero ter um filho com você. Não quero seguir me enganando, não quero fazer algo que logo não possa terminar só por não saber como faço para te deter e que fixe a cabeça em um só lugar. Se vou com você à essa viagem, tal vez termine sendo como um estorvo, não creio que aguente tanto, Chepe. Melhor se sou sincera.
Nos abraçamos na rua, depois chegamos a casa e não voltamos a falar da viagem nos últimos dias que estivemos juntos.
Foi na rodoviária onde me despedi dela. Seria a última vez que veria Hilda.
Entramos em uma cafeteria para tomar o café da manhã e esperar a hora de saída do ônibus que a levaria a casa dos seus pais.
– Não tem fome, quase não comeu nada.
– Me doi a barriga.
Logo me olhou e voltou a chorar…
– Cuide–se muito, cuide–se muito. Coloque um espelhinho na bicicleta, consiga um capacete, vá devagar e corra muito, e ria todos os dias, desfruta da viagem. Espero que atinja seu sonho, espero que Fabiola consiga entrar na Universidade e seja uma mulher profissional para que ajude as crianças. O amo, o amo... vou te querer para sempre, vou me lembrar de você para sempre com alegria e muito obrigado por me ensinar a andar de bicicleta e por tudo que vivemos juntos. Foi muito bonito viver na Nicarágua, foi muito bonito viver com você... não quero que morra, não me esqueça... E já não entendia mais o que me dizia porque voltou a chorar.
A tive abraçada todo o tempo que nos restava e antes de sair até o ônibus, se agachou, pegou sua mochila, a abriu e tirou uma sacola preta de plástico.
– Toma isso.
– Que é – leva com você – me disse.
Abri a sacola e pude ver a almofadinha que ela havia comprado e por qual eu tinha reclamado com ela.
– Te presenteio, leva ela na sua viagem para que durma bem quando estiver muito cansado de pedalar.
Então voltei a abraçar ela e ficamos assim. Sem falar. Escutamos que ligaram o motor do ônibus em que ela iria e nos despedimos.
Ela pegou sua mochila, caminhou até o ônibus, subiu e se sentou na terceira fila de poltronas, do lado da janela. Me olhou e sorriu, mas com um gesto triste e com a sensação de que seguiria chorando.
Se foi.
Durante esses anos de viagem em bicicleta, a maioria das coisas com as que sai de casa se foram acabando. Os sapatos abriram, a roupa perdeu sua cor e foi gastando pelo sol, os utensílios de cozinha e ferramentas agora são outras, a bicicleta é outra e eu também, em boa parte, sou outro Chepe. E outras coisas perdi e esqueci por aí.
Mas a almofadinha ainda segue comigo.
Não quero perder nem esquecer. Ela tem um valor sentimental muito especial para mim. A vejo e noto que já está um pouco rasgada, algumas lantejoulas caíram e também está ficando um pouco pálida.
Depois de olhar para ela, a coloco debaixo da minha cabeça e sinto que é muito confortável. Assim durmo tranquilo... esperando que uma noite dessas venha me ver em um sonho.


Luzes em seus olhos
Aaaaaleluiaaa, aaaaaleluia... aleluia, aleluia glória a Deus.
Eu estava ali parado frente a um lado da porta de entrada do templo.
Muito comovido olhava a minha filha cantando no coro da igreja, queria gravar em cada instante a imagem dela nas minhas pupilas.
Fabiola era a mais alta de todos os integrantes do coro. E parecia reluzir com seu cabelo negro, liso e comprido. Seu rosto resplandecente e com sua essência que caía bem com a vestimenta que era a adequada e especial para a ocasião como os demais, mas me parecia ver ela mais espetacular. Penso que todos os padres vemos assim a nossos filhos.
Minha mãe também a contemplava com alegria e ares de orgulho no seu rosto, sentada em uma das primeiras cadeiras.
Ao finalizar os cantos e o louvor, o pastor passou para a frente, detrás do púlpito e deu uma breve pregação sobre a importância de manter e lutar por uma família unida, o que contrastava com o que eu faria depois dessa noite de natal, não sabia quando voltaria a estar junto com a minha.
Depois que terminou o serviço, todas as pessoas e os fiéis saíram para suas casas. Nós também fizemos o mesmo.
Minha mãe com a tia Rosa e Maida, a esposa do tio Jeremias, foram aquela noite as responsáveis de preparar a cena natalina, o trio de mulheres que seguramente trairia umas verdadeiras surpresas deliciosas.
Uma grande vasilha com ponche de frutas quente, pão caseiro assado, uma generosa salada de batatas, feijão refoga com guacamole e um pobre peru sacrificado.
O tio Enoc e o tio Edenilson trouxeram seus violões e cantaram antigas canções com Luis, o esposo da tia Rosa. Mas logo, todos terminamos cantado juntos. Era muito bonito estar juntos ali sentados ao redor da grande mesa com os tios, os primos, minha mãe e Fabiola.
Comemos e depois seguimos cantando. Quando chegou a meia-noite, todos paramos e nos abraçamos rindo, fazendo piada entre a gente.
Feliz Natal, feliz natal…
Só fazia falta a mãe da minha mãe e de todos, a vó de todos. Mamachave, que faz uns meses que havia falecido.
E de repente... pom, pom, pom. O som da queima de fogos artificiais. Subimos todos como de costume ao terraço para ver. Eram muito alucinantes e brilhantes todas as luzes de cores, formas e tamanhos em extraordinárias dimensões que iluminavam toda a cidade. Desde cima do terraço se via todo o espetáculo muito impressionante e bonito.
A maioria das casas também brilhavam com suas cordinhas acesas de pequenas luzes natalinas penduradas desde os tetos e ao redor de suas janelas.
E ali estava eu nesse momento com toda minha família surpreendida, com suas caras de felicidade. A queima de fogos não parava e seguia saindo disparados até o céu desde os pátios das casas vizinhas. Parecia muito curioso, como que os vizinhos tinham gosto e alegria ou talvez, muita grana para gastar ao nos presentear aquele show.
Em um momento deixei de ver as luzes e comecei a olhar detalhadamente os rostos de todos os membros de minha família. As tias, os tios, as primas, os primos, seus filhinhos, minha mãe e minha filha.
Pude ver como as luzes se refletiam nos seus olhos, era como poder ver tudo neles, toda a vida mesmo. ali no fundo de seus olhares estavam todos os ancestrais, nos olhando desde o infinito; parecia que por um fugaz instante tudo ao seu redor parou numa pausa nos deixando em um silêncio e em uma sensação mágica... maravilhosa.
Não queria que as luzes terminassem jamais, me satisfazia estar ali vendo todos assim.
Também lembro estar certo de algo mais. Que naquele momento, não queria estar em nenhuma outra parte do universo que não fora ali, junto com eles.
Minha primeira família.


Um poeta em bicicleta
Minha bicicleta era uma Raleigh, do ano 1963, fabricada nos Estados Unidos. Comprei ela por vinte dólares numa loja de coisas usadas. De todas as outras, foi a que eu gostei mais por ter um estilo em particular.
A chamei de “a Cabra” pela forma do seu guidão que, para mim, tinha algo assim como um aspecto de chifres.
Da minha casa na prainha onde morava ao povoado mas próximo havia uma distância de mais ou menos dez quilômetros, eu tinha o costume de ir na Cabra, quase todos os fins de semana, ao mercadinho ou a visitar alguns amigos.
O povoado de Cara Sucia é como o centro comercial do nosso município e a estrada que chega até a fronteira com Guatemala o atravessava.
Algumas vezes os havia visto passar e quase sempre viajavam em par, mas aquele sábado que fui olhei que estava um sozinho por aí, num canto do posto de gasolina na estrada do povoado. Estava tranquilo e sentado numa pedra tomando uma cerveja debaixo da sombra de um almendro7. E ao seu lado, uma velha bicicleta carregada de engenhocas pendurados por todos os lados.
Não pensei duas vezes e me aproximei, dirigindo–me até ali para conversar com ele.
– Oi amigo.
Me olhou e me sorriu com um pingo de tédio. Tinha a cara espinhenta e avermelhada, queimada pelo sol. Logo com um sotaque tonto típico dos gringos disse:
– Oooohhh! Bicicleta bonita.
Hahaha, eu tenho o costume de rir de quase tudo. Soltei uma gargalhada e respondi:
– Oooohhh! Obrigado, amigo.
Me sentei ao seu lado e começamos a conversar. Bebe? Me perguntou. Não, respondi. Não sabe viver cara, murmurou.
Perguntei sobre tudo o que ainda não tinha claro sobre viajar assim. E ele, um pouco tosco mas tranquilo, respondia no seu espanhol um pouco distorcido.
Passava mais ou menos uma hora de diálogo, me estendeu sua mão e disse:
– Foi um prazer conversar com você, vou continuar meu caminho.
Ficou de pé e eu também. Agradeci por seu tempo e um abraço de despedida. O tipo foi, agarrou sua roupa de bicicleta e saiu cambaleando. Vendo–o afastar–se pensei: “caralho, me esqueci de perguntar o nome”.
– Hey, hey! Comecei a gritar. Amigoooo. Hey! Espera.
Peguei rapidamente a Cabra, subi nela atrás do gringo bêbado. Quando por fim o tive perto, disse:
– Desculpa irmão, só queria saber como se chama.
O velho reduziu a velocidade deixando de pedalar um pouco e me respondeu:
– Charles Bukowski, mas gosto que me chamem de Bukowski, o poeta de merda, hahahaha... – ficou de pé na bicicleta e começou a pedalar forte, mais forte e foi garalhada como um esquizofrênico, afastando–se muito rápido de mim.
Eu parei um pouco confundido pensando: “ah, é outro louco”.
A raiz desse último encontro com esse tipo de pessoas e dado que a maioria da gente (os seres humanos) fazemos o que vemos imitando o que os outros fazem, comecei a planificar minha própria viagem. Já o vinha tentando anos atrás, mas estava capturado nas armadilhas de um amor. Logo passaram outros anos e não saí devido ao triste falecimento do meu pai. Outros anos mais tarde, já quase decidido, volto a crer na ilusão de outro sentimento e assim me foi passando o tempo, até que chegou o dia, quinze anos depois.
Fazem uns pouco meses, em uma loja de artigos usados, encontrei outra bicicleta Raleigh, mas de um modelo mais novo e claro, mais caro, mas num estilo feminino. Seu guidão não era como o da cabra, tinha quadro de alumínio com um estilo de uma menina magra. E depois de pensar vários dias, a chamei de “Delgadina”.
Eu mesmo fabriquei a garupa dianteira e com mochilas e sacolas de comprar verduras amarradas, saí de casa com um orçamento de 40 dólares no bolso da minha camisa, ao lado do coração.
Uma manhã, na metade de janeiro de 2018, comecei a pedalar. Com uma pinta ingênua e pose de aventureiro, comecei a viver minha liberdade, a alegria e a emoção me entregando ao despojo. Os primeiros cem quilômetros me levaram dois dias para fazer e foi muito desgastante. Entretanto me enchi de orgulho e considerei que tal proeza merecia a primeira foto da minha viagem. Nos dias seguintes já me sentia como “o Quixote da bicicleta” e quando notei ao passar em frente às pessoas, minha imagem chamava atenção, tinha um leve estilo de arrogância. Alguns motoristas de carros buzinavam e me cumprimentavam ao passar. Eu lhes respondia fingindo estar um pouco desinteressado para que pensassem que vinha viajando desde muito longe. Era tudo muito divertido, era meu início e o melhor de tudo era que eu gostava do que estava fazendo. Aos poucos dias cheguei na minha primeira fronteira e, com pinta de altivez, afirmei ao policial que pedia os documentos que sim, que estava viajando em bicicleta pela América Latina e pela primeira vez contei da minha aventura a alguém.
E de aí em frente, as primeiras experiências pouco a pouco começaram a chegar.
Entardeceres espetaculares tanto nas praias como nas montanhas, amanheceres musicais e cantos nos concertos das aves, noites iluminadas por milhares de vaga–lumes e estrelas, fogueiras rodeadas de novos amigos. Tudo era belo e comecei a desfrutar mais cada instante de minha existência nesse mundo. Aprendi a desfrutar e a saborear cada comida, cada nova fruta que provava. Também desfrutei cagar livre e placidamente nos arbustos ou na beira da estrada. Me introduzi na busca de viver com intensidade cada momento e cada coisa por mais efêmera que parecia e quando alcançava encontrar um ponto, um lugar, um canto onde passar a noite e armar minha barraca, entrava nela muito contente e dormia em conforto total.
As poucas vezes que me encontrava na rodovia com outros cicloviageiros, sempre parava, colocava a bicicleta de lado e corria até eles para abraçar–los como quem se encontra com um velho amigo. Logo lhes dava o endereço da minha casa se por um acaso um dia passassem por aí.
Sentia um grande encanto pelas músicas populares de cada região que ia passando, também me atraia escutar seus sotaques marcados, seus assuntos cotidianos de conversa tudo, tudo... abri meus ouvidos para escutar tudo. E até me encantaria escutar o gemido luxurioso de uma dama em pleno orgasmo.
Arco–íris, rios frescos e limpos, brancas e belas nuvens em um celeste céu. Rostos de lindas mulheres e humildes, risadas de crianças, histórias de anciões, novas e diferentes verduras, queijos, doces, bebidas alcoólicas de todo tipo e cor. Infinidade de coisas novas.
Tudo isso, tudo o que estava apenas vivendo, me fez sentir muito bem, me senti mais saudável e em boa forma, me senti mais humano, mais jovem, mais moleque, mais feliz.
Tudo isso era mais lindo e bonito do que me havia contado aquele louco gringo poeta de merda.


Uma lenda viva
Uma nota de dez córdobas era tudo que me restava de dinheiro, depois que paguei a passagem da balsa para chegar até Ometepe. Até o momento, tudo que vivi havia sido fantástico.
Meu plano seguinte era trabalhar em uma ilha por uns meses e com o que ganhar poder cruzar Costa Rica por toda a costa até chegar no Panamá.
Quando cheguei ao porto, a balsa “Che Guevara” já tinha o motor ligado mas consegui entrar com a “Delgadina” a tempo, minutos antes que zarpasse.
Coloquei a bicicleta em uma esquina, perto da comporta de desembarque, encontrei por ali tirado um pedaço de corda e a amarrei contra os canos da varanda para que no caísse com o movimento das ondas. Logo, caminhei entre os caminhões e os carros e fui sentar sobre o rolo de cordas na proa. Queria estar ali para mirar Ometepe durante todo o tempo que durasse a viagem.
“A ilha mãe” eu a chamo. Porque os dois vulcões juntos que tem, a meu ver, simboliza os seios de uma mulher. E além, nos havia alimentado a mim e a minha família por dois anos, também nos deu vida.
Fui o último a descer da balsa quando chegamos ao porto de Moyogalpa. Soltei a minha Delgadina e saí caminhando junto com ela. Dessa vez chegava na ilha sozinho, minha bicicleta e eu. Chegamos ao hotel de dona Nora Gomes, o hotel mais bonito e mais antigo de toda Ometepe. Havia trabalhado para essa senhora por mais de um ano e por isso, quando entrei, voltei a sentir como em casa. Na entrada da sala de recepção me encontrei com Monguito, sobrinho da dona Nora que trabalhava como recepcionista.
– Olha, voltou o Chepe.
– Oi Monguito, como tá?
– Um pouco atarefado cara, me conta, veio a trabalhar ou já vai como andarilho.
– Hahaha, pois sim quero trabalhar um tempo, se lembra que isso já havíamos conversado antes…
– Sim cara. Bem, a mamita8 tá lá pela piscina tomando café. Primeiro vai falar com ela. Ei! E essa bicicleta?
– Ah, Delgadina, sim. Ela é minha companheira de viagem.
– Pois deixa ela por aí e vai ver a chefa, bem-vindo Chepe.
– Muito obrigada Monguito.
Era um hotel bonito, com lindos quartos, um salão grande para eventos, seu jardim muito bem cuidado com uma variedade de plantas frutíferas nativas da ilha, flores aromáticas que atraiam as borboletas e os beija–flores.
Dona Nora, a mamita como todos a chamávamos carinhosamente, estava sentada na sua cadeira de balanço com uma xicarazinha de café na sua mão. Olhava o que lhe falava Tonhito, o jardineiro mais antigo do hotel. Tonhito varria as folhas secas que caiam do abacateiro.
– Chegou o pata de cachorro – gritou ao me ver –. Te estava esperando Chepe.
– Obrigada, mamita, vim só por um mês.
– Bom, a ver... sim, preciso que trabalhe aqui duas semanas para mim e as outras duas no Istian o hotel de Santa Cruz, anda a pedir ao monguito a chave do quarto quinze e te aloje ali e logo vai à cozinha para que Felipita te dê teu gallo pinto9 e petacones10
Assim era ela, uma mulher nobre, hospitaleira mas também com carácter, com palavra firme e correta. As primeiras duas semanas passei fazendo um pouco de tudo, pintando, consertando tetos e de vez em quando levava a mamita a sua fazenda de café e bananas na sua caminhonete.
Um domingo ao meio–dia saí com a bicicleta até o outro hotel que estava a uma distância de uns trinta quilômetros, ao pé do vulcão Maderas ao sul da ilha.
Passando pela praia de Santo Domingo, um trecho antes de chegar a Santa Cruz onde se localizava o outro hotel na beira do lago e sobre uma grandíssima pedra, estava um sujeito quentando sol como uma iguana. Sem camisa, com calças curtas, careca. Me chamou a atenção que tinha tatuado um macaco com ao rabo enrolado em toda sua cabeça e o outro que me atraiu foi que havia uma bicicleta jogada sobre a areia muito perto dele, com alforjas velhas e desbotadas.
– Oi amigo, me chamo Chepe – lhe disse aproximando–me a ele.
– Oi.
– Tá viajando de bicicleta? De onde vem?
– Do México. Um prazer, sou Hugo Suárez, argentino. Logo nos cumprimentamos com um bom aperto de mão.
Me contou que havia pegado um voo de Buenos Aires a Panamá e a volta seria da mesma forma. No aeroporto montou sua bicicleta e viajou por toda América Central até chegar a capital mexicana, tudo pela costa atlântica e o retorno vinha fazendo contornando todo o Pacífico.
Logo, eu lhe contei a parte do meu projeto e as intenções que tinha de chegar até Argentina na bicicleta e me ocorreu perguntar se me permitia compartir a viagem juntos até Panamá, onde ele terminava seu percurso. E notei que sem pensar muito me disse que sim.
– Uma coisa, amigo, que tenho que trabalhar duas semanas mais para cobrar meu salário – comentei.
– Sem drama querido, eu também havia pensado ficar um tempo aqui para relaxar um pouco – me respondeu. E assim foi. Hugo armou sua barraca na praia em frente ao hotel onde eu trabalharia nos últimos dias antes de sair da ilha e de Nicarágua.
Enquanto eu trabalhava, ele passava os dias dormindo e se banhando no lago, correndo de um extremo ao outro, fazendo yoga, alongando os nervos mas as vezes o perdia de vista. Na noite, chegava ao hotel, se sentava no banco do jardim e se conectava ao sinal de internet do hotel e escutava música ou conversávamos sobre a viagem.
Tinha pinta de ter muita experiência em cicloviagem. Nos últimos dez anos de sua vida os organizava de uma maneira equilibrada entre viajar e trabalhar. Seu método dera trabalhar os primeiros seis meses de cada ano e viajar os últimos seis meses. Desse modo, financiava seu estilo de vida. O emprego de guarda-florestal de alguns dos parques nacionais em Bahía Blanca, sua região, lhe permitia levar assim seu ritmo.
Já na última semana, Hugo me contou que estava se entediando muito e que se eu queria, podia me ajudar como pintor. Na noite chamei a mamita para saber sua opinião e ela me disse que estava de acordo com o argentino me ajudando, mas que não receberia pagamento em dinheiro. Que ela oferecia comida e uma cama dentro do quarto onde eu dormia em troca do seu trabalho. Deste modo, passei os últimos dias no hotel com esse careca que todo o tempo me falava do tanto que havia vivido nas suas viagens.
No último dia que trabalhamos, a mamita nos convidou para um almoço especial para os dois.
Dois nacatamales11 de porco para Hugo e dois nacatamales vegetarianos para mim e a indispensável xícara de café orgânico da sua fazenda.
No dia que saímos da ilha, passei para me despedir do seu Gilberto e do seu Evenos, os que haviam sido meus vizinhos.
Lembro com muito carinho as palavras do seu Gilberto quando me disse:
– Chepe, queria te dizer que se um dia decide voltar a ilha e viver aqui, conte com um espaço de terra no meu sitiozinho, aquele terreno do morrinho onde estão os abacateiros que eu te presenteio para que ali faça uma cabana e viva tranquilo.
Quase me faz chorar, voltei a abraçá–lo e lhe agradeci com a alma.
Saímos rumo ao porto quase fugindo. Não disse a ninguém a que horas sairia nem por onde, porque nunca gostei das despedidas com lágrimas.
Tomamos a primeira balsa que estava para sair, subi até o convés do barco e fixei meu olhar para frente, não tive coragem de olhar para trás apesar de sentir a garganta apertada. Logo começou a chover muito forte mas não quis baixar, fiquei ali, me molhando, pensando, até que chegamos ao outro porto.
Entrei na Costa Rica gritando e fazendo aposta de corrida com Hugo.
A primeira noite de viagem dormimos juntos num banco no cais. A seguinte noite armamos nossas barracas debaixo de umas mangueiras, atrás de um posto policial e amanhecemos picados pelas formigas.
Era muito divertido viajar junto com esse louco argentino. Lavávamos a roupa nos rios e tomávamos banho no mar até duas vezes por dia por causa do grande calor, entravamos as escondidas nos parques nacionais que cobravam quinze dólares aos gringos para entrar para ver os lagartos, porcos silvestres, os macacos e guacamayos12.
Cozinhávamos com lenha na beira ca estrada e de risada em risada íamos percorrendo as primeiras praias do Pacífico costa-riquenho até que chegamos a península de Nicoya.
Mas na segunda semana de viajar juntos Hugo Suárez começou a mudar. Uma manhã, muito cedo, ficou de pé em frente a minha barraca e me disse:
– Levanta–se filho da puta, não me diga que quer que eu traga o café da manhã na cama? “Boludo13”.
Isso me fez sobressaltar de susto mas não me movi e segui assim, em silêncio, fingindo que estava dormindo e não o havia escutado.
Olhei que sua sombra se afastou da minha barraca e depois de um bom tempo me animei a sair como se não tivesse passado nada.
Vi que estava ali agachado, soprando a lenha, querendo ascender o fogo com os olhos muito vermelhos pela fumaça. Fui até ele e o cumprimentei:
– Bom dia Hugo – mas ele não me respondeu.
Reconheço que a experiência de ter viajado com esse louco como companheiro teve suas particularidades e de algum modo, tirei proveito.
Tinha suas maneiras e uma metodologia própria de como viajar em bicicleta. Como ética, não encher o saco de ninguém; como ele o dizia, tomar banho e lavar a roupa só em águas públicas como nos rios ou lagos. Odiava o conceito de “propriedade privada”, por isso preferia dormir no mato, em casas abandonadas, nas rotatórias das rodovias, debaixo das pontes, parques, campos de futebol, onde fosse que agarrasse a noite. Qualquer lugar que não necessitasse falar ou pedir permissão a alguém. Dizia que parecia uma estupidez pagar para dormir, para cagar, para falar ou para fazer amor. De dia comíamos somente as frutas que encontrávamos pelo caminho e somente cozinhávamos na noite, fazendo uma janta bastante generosa. Tava seguro de ser um resultado da sociedade apodrecida que nos rodeava e que os dez anos viajando de bicicleta por vários países o havia feito duro.
Isso é o que sou – dizia a si mesmo rindo só – eu criei a mim mesmo. E logo soltava uma forte e sarcástica gargalhada. Hahahaha…
Quase todo o tempo escutava música a todo volume no seu celular, sua banda favorita (Soda Estereo), escutava inclusive quando dormia. São os melhores, dizia a cada momento, de tanto escutar as mesmas músicas cheguei a decorá–las, inclusive já sabia que canção continuava, uma atrás da outra.
O estranho é que cada dia se soltava um pouco mais, entrava na sua barraca e perguntava e falava sozinho.
– Que vou fazer Huguito, querido? Já tem duas semanas sem se masturbar e quase dois anos sem fazer amor com uma mulher.
Desde fora o olhava e parecia que girava como um rolo de um lado para outro dentro da barraca, e quando parava começava com um monólogo improvisado.
Eu, ao princípio, tinha uma sensação de pânico, logo comecei a rir e saia a caminha, o deixava ali convivendo com seu abandono.
– Que vou fazer? Gritava na meia-noite, minha namorada se foi para Alemanha, suas linhas me amarram o pensamento e prendem minhas memórias. Solta–los cadela, dê-me–os, corta o anzol por favor. Tenho uma tatuagem abstrata na minha cabeça que zoa comigo quando durmo e se escapa na busca de uma amante, se aproveita da minha calvície. Mas seu medo, por seu medo que fiquei sem cabelo aos vinte cinco.
Se calava e depois começava a falar em voz baixinha.
– Estou viajando com um índio negro, cavernoso, índio de merda que está barrigudo de tomar tanto óleo. Gordo que viaja de bicicleta usando uma calça de vaqueiro e que parece um caminhoneiro. Hahahaha. Minha vida é assim... promiscua em trivialidades. Mas é estranho, porque o indio lê livros, mas quando me mostra descubro que lê pura pendejada14, é um pendejo o índio…
Na manhã, quando se levantava, me perguntava como havia sido minha noite. E logo me dizia que se havia escutado ele falar dormindo.
E enquanto íamos viajando nas bicicletas chegava do meu lado e me dizia:
– Se falei enquanto dormia de noite não dê atenção negro15. Olha, li as notícias da Argentina, um amigo me mandou. O peso não vale nem merda, quarenta pesos por um dólar, esses são os políticos corruptos, só são cabelos e bundas, bundas gordas, magros com cabelo ou sem cabelo isso são, uma bolsa de merda, que se vão a “concha de la lora”16. Por isso você, melhor trabalhar filho da puta, não seja um político mantido.
Eu não respondia, somente o olhava, as vezes o admirava e as vezes o odiava, mas não me assustava, mas bem o tinha uma certa compaixão.
Quando via os passarinhos, parava e tirava foto enquanto os puteava. E assim se acalmava um pouco da sua esquizofrenia.
De vez em quando parava em frente a um armazém e me preguntava:
– Ao cavalheiro te gostaria tomar uma fria e merecida cerveja?
– Sim, claro Huguito – respondia.
Nos sentávamos no chão fora do armazém e a tomávamos.
– As vezes existem ocasiões que vale sair do orçamento – me dizia muito sorridente.
E começávamos a conversar.
– Olha, negro, não esqueça que tem a alegria de viajar com uma lenda viva.
– Sim irmão – respondia rindo–me –. Muitíssimo obrigado.
– Olha índio, não seja esses “caras de mocinhas” que se dizem que chamam “cicloturistas” que andam buscando pouso e sexo grátis pela internet. Lembra que ninguém tem a culpa que faça o que fizer. Esses moleques com bochechas de bundas de bebê querem viajar e ser felizes as custas dos demais e depois vão publicar nos seus Facebook fotos bonitas, em lugares bonitos, junto com mulheres bonitas. São uns mantidos, uns enche saco e uns boludos17.
– Eu, por isso meu negro, eu por isso prefiro me foder trabalhando nesse maldito parque para poder viajar livre de lástimas e de vergonhas e sem incomodar. Compro meu amendoim e minha cerveja, não a peço a ninguém. E eu também dessa forma reivindico minha dignidade para que não venha um dia a me dizer um desses políticos de merda que Hugo Suárez não tem direito a estar louco.
Desdo o primeiro dia que viajamos juntos sempre ele foi na frente. Um dia que ia peidando uns peidos muito fedidos, acelerei e me coloquei a frente. Começou a rir e logo me chamou: índio, índio, pára, vem. Parei e fui até ele.
– Bem, negro, reconheço que é mais jovem e com mais energia que eu, mas ingênuo. Hoje te vejo animado a correr, se quer siga, livre–se, voa passarinho... eu verei se te sigo.
Não lhe disse nada. Montei na Delgadina, comecei a pedalar e fui, fui... fui deixando ele para trás, um, quatro, cinco quilômetros. Sem olhar para trás segui... o deixei.


Animaizinhos de papel
Não seu porque motivo não pude conhecer a muitas pessoas em Costa Rica, ou pode ser que quando continuei minha viagem solitário me dediquei um pouco a observar introspectivamente o meu ser interior. Cada dia procurava chegar a dormir nas praias mais longes onde pudesse estar sozinho e fazer uma fogueira, amarrar minha rede e assim, tranquilo, meditar com o vento, os grilos e o som do mar. Junto ao fogo, tratando e buscando no olhar dos meus olhos o brilho da lua e das estrelas, até o mais fundo de minha mente e meu consciente e me sentia bem nesse ritmo de vida. Mas conheci a duas pessoas que para mim, dignas de mencionar.
Empenhado por percorrer na bicicleta e conhecer toda a borda do oceano Pacífico costa riquenho, uma tarde quase de noite e depois de conseguir sair de uma estrada de terra um pouco barrenta, consegui chegar a Golfito, a cidade de um porto nada tranquilo em comparação do que vinha desfrutando os dias anteriores.
Estando ali notei que era uma cidade de muita atividade noturna, assim que pensei em sair dali e buscar um lugar onde armar minha barraca a uns cinco quilômetros fora da cidade, mas quando ia saindo, olhei ao lado da rua um pequeno kiosco18 onde vendiam café e hambúrgueres. Tive a ideia de ir até ali e perguntar se conheciam o tinham ideia de um lugar onde poderia dormir.
Lembro que havia um taxista tomando café e foi até ele e perguntei a respeito, mas me disse que não sabia nada e que somente havia chegado a cidade para deixar um cliente.
Uma das duas mulheres que atendiam no negócio me perguntou de onde eu era e para onde iria. Depois de contar um pouco da minha história, me aconselhou não seguir de noite, porque parecia muito arriscado. Me disse também que ela trabalhava o turno até amanhecer e que se queria podia armar minha barraca atrás do kiosco. Para mim foi fenomenal aquele conselho e me dispus a obedecer.
Não sei, talvez lá por uma da manhã senti que alguém movia minha barraca e dizia: oi, oi…
– Que aconteceu? – respondi. Me levantei, abri o zíper da barraca e coloquei a cabeça para fora. E me dei conta que era a mesma mulher do kiosco.
– Oi, sou a boa samaritana – disse.
– Queria te perguntar se já comeu algo ou se tem fome.
Fique ali olhado ela um pouco dormido e respondi que sim.
– Bom, saia daí e venha comer algo, eu convido. Sou Arlet, um prazer.
– Me chamo Chepe, o prazer é meu.
Ainda de vez em quando escrevo para Arlet, aquela mulher do porto de Golfito na Costa Rica, que teve uma bondade de ir me acordar essa noite para me dar de comer, ela é um dos meus anjos e que cada vez que posso, a agradeço o ato de amor que fez por mim.
A Wilber Rodriguez o encontrei de maneira diferente, mas também pela noite. Depois de convencer o jovem administrador do restaurante “El Camaronal”, um lugar na beira da estrada e muito perto de um grande rio, esse cara me indicou que o único lugar que me podia oferecer para dormir era atrás de uma parede ao lado do estacionamento de carros.
Eu estou acostumado a pedir as pessoas de um favor para não deixar a imagem de ser um aproveitador, mas já acostumado e pensando várias vezes, por fim decidi correr o risco de ir de novo e busca o jovem administrador para lhe pedir se me deixava carregar meu celular e me disse a senha da wifi.
Saí da barraca a caminho do restaurante e cumprimentei um tipo que ia saindo do seu automóvel recém-estacionado; por sorte, ao administrador não parecia nenhuma moléstia minha necessidade.
Estava ali no balcão do bar esperando que meu celular se carregasse um pouco, quando o tipo do carro se aproximou do bar e pediu ao jovem uma cerveja, logo se dirigiu a mim dizendo–me:
– Escuta amigo, gostaria de te convidar para beber algo, não pense que te julgo, mas noto na cara muito queimada pelo sol e também tem o aspecto de estar muito cansado.
Recordo que depois de escutar–lo quase me jogo para abraçar–lo de alegria, mas só respondi que minha aparência não o estava enganando. Cinco minutos mais tarde estávamos tendo uma conversa como dois velhos amigos. Saímos do bar e sentamos em umas cômodas cadeiras de baixo de um quiosque de folhas de coqueiro, com uma mesa perto da gente.
Me chamou a atenção o colar que pendia do seu pescoço e a pedra que tinha amarrada. Era uma pedra de jade com um verde misterioso e um desenho de origem ancestral.
O perguntei como a havia conseguido e o que essa pedra significava para ele. Não lembro a história de como a conseguiu, mas me disse que ele creia que essa pedra de jade tinha boas energias e que já trazia consigo fazia vários anos.
Tomou um gole de cerveja bastante prolongado, colocou a garrafa vazia na mesa, me olhou, sorriu e disse:
– Chepe, amigo, eu estou feliz com a vida que tenho, é por isso que trato de desfrutar cada instante de minha existência neste mundo. Te conto que me encantaria pescar. O outro dia estive com a vara de pescar toda a manhã até depois do meio dia provando a sorte no rio, mas nada mordeu, a pesca é assim, a vida é assim, dias de pescar peixes, dias de pescar experiências. Chegando em casa vejo minha filhinha, a mais pequena, que estava fora do lado da porta sentadinha numa cadeirinha de madeira que eu fiz para ela uns dias antes. Também havia uma mesa pequena e parecia estar como cortando jornais e revistas as quais tirava de uma caixa de papelão. E como já conheço algo de como é essa pequenina, me aproximei atuando com humildade. Oi senhorita.
– Boa tarde, senhor.
– Olha, aaa, desculpe a pergunta, que faz aqui?
– Olhe senhor, te conto que estou começando meu próprio negócio.
– Ah! E você pode ser tão amável de me contar de que se trata o seu negócio?
– Sim, claro senhor, é lógico. Olhe, eu vendo animaizinhos, animaizinhos de papel. Tenho ovelhinhas, franguinhos, uma vaca, um cavalo, um patinho e um galo.
– Uuauuu!... que bom negócio, e quanto custa os animaizinhos?
– O que não tem cor a cem colones19 e os que já estão pintados a duzentos colones, mas como hoje a venda não está muito boa, dois coloridos a trezentos colones.
– Bem, veja, espere um segundo, aqui está, sim... me dá um franguinho, o patinho e mmmm, deixa eu ver... sim, me dá a ovelhinha.
– Muito bem, tome os animaizinhos, são trezentos colones.
– Sim, como não. Tome, aqui está. Quinhentos colones.
– Desculpe senhor, mas não tenho troco.
– Não, não, não... está bem senhorita, te deixo de gorjeta.
– Bem, você é muito amável, muito obrigada, que tenha um bom dia.
– Como não vou ser feliz com minha vida Chepe? Se cada dia déramos conta dos detalhes e as coisas bonitas que acontecem ao nosso redor... Chegar em casa, encontrar essa pequena que confia em mim, crescendo com a seguridade que o papai acredita nela e que a apoiará em tudo o que faça na sua vida. Não tenho muito dinheiro Chepe, mas sou mais que um afortunado com essas coisas simples, mas bonitas. Assim me sinto.
Wilber me mostrou a valorizar os instantes, os momentos com nossos filhos. Que as vezes, por andar preocupados pelo trabalho ou em qualquer coisa, não vivemos com intensidade os detalhes bonitos que nos presenteia a vida. A história de Wilber, me mostrou que muitas vezes somos tão bobos, que não aproveitamos a oportunidade que nossos filhos nos dão para voltar a inocência, a simplicidade em que é mais fácil ser feliz e que tão rápido nos esquecemos que fomos crianças.
Um dia desses, Wilber me mandou uma foto numa mensagem de WhatsApp onde está ele e sua pequena pescando em um rio. E debaixo da foto me mandou um áudio. Era a menina que dizia:
– Oi Chepe, que tenha uma boa viagem... te quero muito.


2:05 AM
Não tenho sono, não posso dormir. Saí da barraca, vim sentar debaixo da luz de um poste de eletricidade e escrevo isso... pensando em você.
Conhecer-te é um dilema, dependendo do ponto de vista de onde o veja.
Quando ela te pergunta algo, o qual já sabe com antecipação a resposta, fazer um gesto e um olhar que te faz voltar aos dezesseis anos.
Somente você tem um tom e um sotaque em particular e suas palavras não faltam nem sobram, tudo é coerente no seu dialogo.
Somente quando está ao seu lado pode sentir sua energia celeste claro e ainda que os incrédulos nos digam que as cores não se podem sentir, é nela onde se rompem os esquemas.
E quando te abraça (se tem essa bendição) não se senti morbidez, nem julgamentos, nem preconceitos20 e não tem necessidade de cheirar bem, porque ela só respira seu cosmo.
O tempo para ali dentro dos seus magros braços, nos bracinhos da sua alma.
A seu lado todas as músicas românticas se tornam mais românticas... inclusive as alegres.
É alegria e desgraça. “Por desgraça”, porque te deixa um vazio desgraçado quando se vai e te deixa.
“Por alegria”, a imensa sorte de ter–la conhecido.
Eu a vejo e logo penso.
Deveria ter um nome divino ou todos os nomes e adjetivos das mulheres mais prodigiosas da história da humanidade.
Entretanto existe a possibilidade que WENDY. Signifique todo ele…
San Bito, Costa Rica
Domingo, 2:05 da manhã.


A campainha dos milagres
Das sete bicicletas exibidas para venda fora da loja de coisas usadas, a “Delgadina” foi a que gostei mais.
Quadro de alumínio, roda vinte oito, um banco estreito, cômodo e de boa qualidade, sistema de marchas simples, uma garupa traseira resistente e uma altura ideal para mim. Era a melhor para os percursos longos.
Bem, toda ela como fabricada para poder viajar tranquilo por muito tempo, incluso seu desenho feminino me encantava, assim, até podia mijar na beira da estrada sem descer da bici.
– Ela é uma linda bicicleta – me disse a velhinha que as limpava e atendia os clientes nessa área da loja –. Essa bicicleta leva como quarto anos aqui e não se vende, cada meio ano nos trazem uma vinte bicicletas dos Estados Unidos. Vem inclusive umas que não são tão boas e se vendem mais rápido, mas esta ficou trancada por aqui.
– Eu gostei – comentei –, e qual é o preço dela?
– Pois não é tão cara – respondeu – cento e vinte dólares. Já vendemos bicicletas até quinhentos dólares e pode ser que em piores condições. Eu, na verdade jovem, não entendo muito de marcas de bicicletas, mas aqui vem pessoas que quando as veem desde fora sabem o valor, que para eles, pode custar. Essa bicicleta é bonita, aí como a vê. Sim, mas tem algo de especial em particular, ela tem seu toque mágico.
– É sério? Qual é? – lhe perguntei com um sorriso curioso como me antecipando como se a anciana estivera utilizando uma estratégia barata comigo para vender.
– Não ria, filho – me advertiu –. Tô falando sério, a virgenzinha não me deixa mentir. Olha, vem aqui, essa campainha que tem, as vezes soa sozinha. O outro dia que eu estava limpando as bicicletas, não sei como foi, mas tô cera que a ouvi que soou mas bem baixinho. Bem, e isso ficou na minha cabeça, presta atenção não me assustei, pensei que a havia deixado mal colocada ou muito grudada com as outras. Mas depois fui que descobri qual é o mistério que tem. Tenho uma neta de dezenove anos, que quer estudar na universidade. Meu filho, o pai dela, morreu faz sete anos, um caminhão o atropelou e desde então eu ajudo minha neta para que estude, por isso trabalho aqui limpando as coisas. Então no outro dia andava com preocupação na minha cabeça de que não tenho tanto dinheiro para pagar uma universidade a minha neta. Bem, cheguei na manhã aqui e comecei como sempre a limpar as bicicletas. E quando chegou a vez de limpar essa disse a mim mesma: já não quero mais me preocupar com o estudo da minha filhinha, melhor eu deixar nas mãos de deus, confio que ele nos ajudará. Logo rapidamente me senti muito aliviada deixando meus pesos a Deus, e de alegria me passou de tocar essa campainha, rrin, rrin…
– E que aconteceu depois?
– Hihihihi, claro muchacho21, foi incrível mas certo, nessa semana minha Lupita saiu na lista das cento e cinquenta senhoritas bolsistas pelo programa de educação do país.
– Que bonita história madre22! Obrigada por compartir ela comigo. Espero um dia escrevê–la. Te conto que eu também tenho uma filhinha que quer ir estudar no exterior e por isso estou comprando uma bicicleta, para ir na frente e lutar pelo seu sonho.
– Tenha fé filho, que vai conseguir.
– Sim, madre, isso espero e já me decidi, vou comprar essa bicicleta, a da campainha dos milagres.
– Hihihi você leva um tesouro, muchacho.
Foi em São Bito, Costa Rica, onde pela primeira vez toquei essa campainha para encontrar um lugar onde dormir.
Já me sentia cansado de vir viajando pela costa e cheguei nesse lindo povoado na montanha com a intenção de ver um borboletário e a variedade de beija–flores e orquídeas que tem na região. Muitas pessoas me havia recomendado, mas para chegar em São Bito, tem que subir muitos morros (creio que a metade do caminho o fiz empurrando a bicicleta). A uns três quilômetros antes de chegar a São Bito, me lembrei da velhinha e da magia que podia ter a campainha e quis provar. A fiz soar duas vezes desejando com todo meu coração encontrar no povoado um lugar para dormir e descansar.
Não tem porque crer no que te vou contar, mas quando cheguei a esse belo e tranquilo povoado na montanha, na primeira cafeteria que entrei para tomar um café, a dona, depois de uma breve conversa que tivemos, me contou que na igreja de San Francisco de Assim tinha um albergue para os migrantes e também estava convidado a tomar café da manhã na sua cafeteria no dia seguinte.
Fui ao lugar e era uma morada ao estilo de uma casa antiga de fazenda, toda de madeira muito bem conservada, com um grande jardim e quatro palmeiras enfileiradas de cada lado da entrada, uma pequena fonte ao centro e beija–flores voando por todos lados beijando as flores.
Fui bem recebido, desfrutei de uma boa ducha com água quente, me deram um jantar e me presentearam uma toalha e roupa limpa.
Ao seguinte dia, passei para tomar o café da manhã com a senhora da cafeteria, me apresentou seus filhos e netos, tomamos umas fotos e rimos um pouco. Saí desse lugar muito contente…
Desde então, tenho tomado o costume de deixar de sofrer por tudo que me possa tirar a paz, a deixar de me preocupar tanto e confiar que Deus, que o universo, que o tempo ou a vida mesmo fará cargo das minhas necessidades e claro, trabalho por eles e toco a campainha, não tanto para pedir algo mas sim como um ato de agradecimento antecipado.
Assim foi que coberto dessa confiança segui tranquilo até a fronteira com Panamá, sabendo que já não tinha muito dinheiro comigo, mas que tudo seguiria fluindo bem.
O tipo que estava sentado em frente ao computador do outro lado do vidro sorriu de modo cético quando disse a ele que queria chegar na Argentina em bicicleta, levantou seu rosto, me olhou com seus olhos vesgos e me disse:
– Você quer imitar os gringos, verdade? Eles o fazem, viajam, vão onde querem nas suas casas móveis, motocicletas, inclusive também em bicicletas, mas eles levam dinheiro nos seus cartões de crédito e bem, seguramente tem mais conhecimento a respeito que você.
– Isso é verdade – respondi.
– Mas você me disse que somente leva oito dólares no bolso da tua camiseta para entrar e sair do Panamá, quando esse país exige comprovar que o orçamento mínimo que deve mostrar um turista são quinhentos dólares…
– Isso também é verdade – voltei a responder com o olhar baixo.
E aí, de repente, me ocorreu de fazer algo. Aproximei um pouco mais a bicicleta até mim e lhe perguntei.
– Você acredita em milagres?
O tipo sorriu de novo, mas agora um pouco mais sarcástica e me respondeu:
– Eu creio que o dinheiro faz muitos milagres.
– Pode ser – lhe disse –, mas também essa campainha que levo com minha bici, presta atenção que descobri que quando na verdade preciso de algo, o desejo com todo meu coração e toco a campainha, as coisas acontecem. É como mandar um WhatsApp a Deus e acontece o milagre, te mostrarei nesse momento, a tocarei na sua frente para pedir um. Rrin, rrin…
– E que pediu? – me perguntou
– Que seu coração se abale um pouco e me deixe passar e lutar pelos meus sonhos –  respondi.
Desta vez, ele baixou o olhar, voltou a sorrir, mas desta vez com um tom de simpatia, escreveu no computador, pegou meu passaporte e o carimbou.
– Toma louco, tem seis meses para permanecer dentro do Panamá. Bem-vindo, mas comporte–se bem e que tenha uma boa viagem. Que passe o próximooo! – gritou.
Sai dali, fui tomar uma foto com a Delgadina de frente com o letreiro de “Bem-vindo ao Panamá”, procurei um restaurante, entrei, comi e paguei quatro dólares e com os outros quatro que me restara subi na minha bicicleta e fui o mais rápido que pude.
Não podia crer, havia funcionado de novo, me lembro que vinha por toda rodovia gritando. Uuuu!!, cantando, sim, sim, sim... obrigado, obrigado, encharcado de adrenalina e suor. Dava duro, seguia pedalando ainda com a dor nos pés, não me importava, estava ali, havia conseguido. Segui o rumo e logo senti a vibração do celular, encostei na beira da estrada, parei e atendi, era minha mãe.
– Filho, onde tá? Como tá?
Eu muito contente gritei:
– Mãeeeee!!! Já entrei no Panamá, estou muito bem não se preocupe.
– Bendito Deus! – me disse ela – Hoje me levantei muito cedo porque sentia uma palpitação e comecei a rezar por você – e logo falamos um bom tempo muito felizes.
Me dei conta que desde a fronteira havia pedalado uns quarenta quilômetros sem para debaixo daquele grande calor sem me dar conta. Então descubro do lado havia uma arvorezinha de marañón23 com somente um marañón maduro.
Fui até a arvorezinha, cortei a fruta, me sentei de baixo da sombra da arvorezinha e o comi.
Secando as lágrimas dos olhos.

O sol de Pedasi
Tendo percorrido um pouco o povoado cafeeiro de Boquere, decidi descansar um pouco na praça central, era muito bonita, com estátuas de aves nativas e muito jardim. Notei que entre os turistas que andavam caminhando por todo o parque, alguns também como eu, buscavam uma sombra para relaxar. Também era notável as pessoas que usavam roupas tradicionais, mais nas mulheres que vestiam uns vestidos listrados coloridos e uns desenhos de flores e aves bordadas a mão. Mas algo que me chamou a atenção foi que a maioria delas andavam bastante bêbados, inclusive as mulheres. Fiquei um pouco triste por isso.
Uma hora mais tarde, mais ou menos, se aproximou um jovem de uns vinte e cinco anos me perguntar de onde vinha viajando com a bicicleta. Depois que conversamos um pouco, me contou que ele conhecia uma pousada que dava alojamento aos viajantes em troca de trabalhos voluntários. Para mim, aquela notícia era muito oportuna, assim que pedi ao jovem se podia me dar a informação de como chegar até aí. Foi tão amável que ele mesmo me levou e me deixou na frente da porta da pousada.
Os donos eram um casal de jovens, na verdade muito jovens para mim para ter uma pousada tão bonito, não era como eu esperava, pensava encontrar algo como uma pousada para hippies ou algo assim, mas sim o lugar era todo moderno com quartos climatizados, umas camas novas, um chuveiro fantástico com banheira, uma sala espaçosa, a cozinha com toso os utensílios necessários para preparar toda uma cena de natal para várias pessoas. Tinha tudo, parecia mais a casa da amante de um prefeito.
A entrevista com eles não durou muito, foi breve e sem nenhum drama.
Dariam-me  cama e comida em troca de ajudar a pintar umas grades de outra pousada muito perto dali, o qual estava a poucos dias de inaugurar. Quando soube disso, fiquei mais surpreendido, mas bem, supus que na melhor das hipóteses eles eram herdeiros de alguns pais muito responsáveis e generosos.
Passado três dias de estar com os jovens na pousada e ter terminado com a parte do meu voluntariado, comecei a me entedia então disse que gostaria de tirar folga no final de semana.
– Sim, Chepe, tira, faça isso – me disse a jovem –. Olha, já que está aqui, aproveita para subir o vulcão Baru. Isso você não pode perder.
E claro fazendo caso e não ignorando essas palavras, foi o que fiz. Acordei cedinho esse sábado e saí da pousada comecei a caminhar. Me afastei do povoado, busquei a trilha e comecei a subir; como na metade do caminho para chegar no topo do vulcão, parei para descansar. Desde aí se via o povoado de Boquete, muito bonito, com seus telhados de telha e suas paredes de diferentes cores. Por todos os arredores se viam grandes árvores floridas, cipreses24 e pinheiros de diferentes classes.
Era sem dúvida um dos povos mais bonitos dos que havia estado na minha viagem, continuei caminhando e ao chegar justamente no topo, encontrei com outros turistas que estavam por ali sentados nas pedras ou na grama, exaustos.
Tudo aquilo estava coberto de uma fresca e plácida neblina, mas ao pouco tempo a neblina começou a desaparecer deixando assim, relevando a todos pouquinho a pouquinho, um impressionante panorama.
Aquela era uma das melhores vistas mais espetacular que eu havia observado na minha vida, pois desde ali e a essa altura se podia contemplar uma boa parte da costa do oceano Pacífico e a outro lado, depois de uma vasta cordilheira com bosques tropicais, a beira do oceano Atlântico. Era um momento verdadeiramente impressionante, todos que estávamos em cima do vulcão ficamos pasmados e maravilhados.
Nesse momento chegou em mim um grande desejo de voltar para a costa e amarrar minha rede numa dessas lindas praias.
Baixei ao povoado com alegria e nessa noite comparti minha experiência com os jovens e os agradeci muito a jovem dona da pousada por me ter sugerido tal lugar mágico. Avisei que sairia no dia seguinte rumo a costa.
– Estamos de acordo, Chepe. Vai a costa, mas chega a Pedasí, ali está uma das praias mais bonitas e tranquilas do Panamá.
Na manhã seguinte, me despedi deles e saí de novo na bicicleta muito emocionado de chegar de novo no Pacífico, já tinha outra ideia do meu seguinte lugar bonito para conhecer.
Pela primeira vez desde que saí de casa tinha ficado sem dinheiro, o único que tinha era, a metade de uma nota de vinte dólares, o trazia como uma recordação de um momento especial. Vinha guardando ele em uma velha carteira na mochila de roupa. Sabia que para chegar na praia de Pedasí tinha que pedalar pelo menos uns dois dias.
Os jovens da pousada me deram uns biscoitos e uns saquinhos de chá de canela e duas laranjas, mas lá pelo meio dia só me restaram os saquinhos de chá;
Antes de sair de David, uma das maiores cidades do Panamá, passei colhendo mangas de uma árvore que estava no caminho, assim passei o primeiro dia. No dia seguinte saí cedinho a rodovia, mas lá pelas dez da manhã já me roncava o estômago de fome, o momento havia chegado, tinha que ir esquecendo a vergonha.
Já havia pensado antes que um dia isso aconteceria e até havia feito alguns treinos sozinho. Agora tinha que tentar. Parei na frente de um supermercado, respirei fundo e entrei.
Não via clientes, creio que nesse momento só estava eu e a chinesa que estava atrás da caixa registradora. O curioso é que ela estava correndo sobre uma máquina que tinha uma faixa girando muito rápido, o ato me pareceu muito engraçado e quase solto uma gargalhada, mas me contive e me aproximei para cumprimentá–la o mais amável que podia, mas ela não me escutava não importava o quanto eu subia a voz, já que usava uns fones e estava ouvindo música. Mas após insistir, os tirou sem deixar de correr.
– Que quer senhor?
– Ahhh, olhe senhorita, euuu, como pode ver, eu estou viajando de bicicleta, venho de muito longe e quero chegar muito longe, mas fiquei sem dinheiro e agora sinto muita fome. Euuu, não sei, você teria vontade de me dar algo de comer?
– Fome? Tem que trabalhar senhor para que não tenha fome – disse.
Quase que me cai a cara de vergonha e a garganta começou a apertar.
– Me perdoe – a respondi e caminhei até a saída.
– Ei homem – gritou – volta – senti que havia uma esperança e voltei.
Desligou a esteira, pegou uma sacola plástica e indicou que a seguisse. Começamos a caminha pelos corredores e foi enchendo a bolsa de frutas e verduras. Logo me deu a sacola com os alimentos e me disse:
– Comida suficiente para dois dias, mas tem que encontrar um trabalho senhor.
– Sim, sim... eu buscarei, o prometo – respondi – E muito, muito obrigado!
Ela voltou ao mesmo lugar onde estava, voltou a ligar a máquina com a faixa que girava muito rápido, colocou os fones nos seus ouvidos e continuou correndo.
Saí do supermercado e pelo caminho vinha pensando que a chinezinha tinha muita razão eu tinha que buscar um trabalho ou um meio de gerar dinheiro.
No dia seguinte, as nove da noite, consegui chegar a praia de Pedasí.
Cento e quarenta quilômetros percorridos em um dia (esse foi o meu primeiro recorde), amarrei minha rede num quiosque, coloquei a bicicleta encostada debaixo da rede, a amarrei e me afundei em um sonho profundo.
Um horizonte cinza escuro, quieto e tranquilo. Ao fundo, uma pequena fagulha que muito lentamente se foi convertendo em algo parecido a uma estrela, aumentando sua luz. E de repente algo como chamas que se ia unindo a um só núcleo e foi assim formando um precioso sol vermelho com raios que transpassavam as dispersas nuvens esbranquiçadas, o oceano tornou suas águas de cor escura a uma trocante cor que ia desde um celeste claro a um azul-turquesa.
Até hoje, o melhor amanhecer que pude contemplar. E desde ali, desde a minha rede, com meus olhos apenas abertos, havia presenciado o nascimento de um novo dia.


Deixa ele chegar
Analisando um pouco sobre minha situação financeira e chegando a realidade das circunstâncias, tomei a decisão de deixar de me preocupar pelo dinheiro. A verdade, o que reconheci como prioridades para continuar com minha viagem, era que comer e ter um lugar para dormir vinha a ser o mais indispensável.
O quiosque onde havia amarrado minha rede na primeira noite pertencia a um senhor pescador da área e na manhã do seguinte dia, chegou para averiguar quem eu era.
Disse-me  que ele construiu o quiosque para descansar os finais de semana com sua esposa.
– Fica aqui todo o tempo que necessite descansar – disse –, relaxa e desfruta da praia, aqui estará bem e será cuidado pela princesa Iemanjá, ela te protege, tão pouco se preocupe com a comida. Vê lá? Todos esses coqueiros são livres, não estão em terrenos privados, pode ir e cortar os que necessite e também te direi uma coisa, olha jovem, esse lugar tem uma energia muito boa, vê a tudo ao nosso redor? Não tem mais que esse quiosque e isso é muito importante de aproveitar e vai te dar conta que aqui de fome não morrerá, aqui a comida chega sozinha.
E claro, o primeiro que fiz foi ir cortar cocos, uma tarefa que não me saiu tão fácil porque o coqueiral estava tão perto e os coqueiros tinham uma altura de entre seis e sete metros, mas com uma vara de bambu e com insistência conseguia baixa eles um por um. Também os cocos eram bem grandes e somente podia levar de três em três a cada viagem.
Para a tarde já tinha nove cocos, lenha para o fogo, a roupa lavada e recém cozinhava uma boa panela de arroz.
Me sentia tranquilo com uma grande confiança e que nenhuma preocupação econômica me tiraria a oportunidade de desfrutar essa maravilhosa praia.
Mas outro dia começou a deixar de ter tanta paz e tranquilidade, começaram a chegar os carros cheios de muitos veranistas ao redor do quiosque, colocaram seus guarda–sois e cadeiras de praia, música por todo lado, jovens jogando bola, tudo se tornou alegria.
Sai da barraca, fui abrir um coco e me deitei na rede a olha a multidão. Em um instante se aproximou uns senhores, um casal de idosos, o cavalheiro me pergunta:
– Onde compro o coco?
– Oi, como tá? Olhe senhor, eu não comprei, eu fui cortar ele naquele coqueiro que se lá – e apontei com o dedo o lugar.
– Mas tem mais? – me perguntou.
– Sim, claro, ainda tenho vários por aí.
– Bem, a verdade é que quando o vimos relaxadinho na sua rede e tomando coco também nos deu vontade de fazer o mesmo.
– Não tem problema nenhum, eu abro um coco para cada um e a dama pode descansar na rede o tempo que queira e também a você senhor posso conseguir uma rede. Por aqui tenho outra.
E terminando de atender os senhores chega um jovem com duas senhoritas que também queriam cocos.
– Quanto custa os cocos? Pergunta o jovem.
– Hahaha, não sei meu amigo, bem se vendo por cinquenta centavos, que te parece?
– Me dá três.
E desse modo fui encontrando na venda de cocos uma entrada muito importante, quase todos me pagavam mais do que pedia, alugava as redes e o banheiro para as senhoritas. E fui fazendo o mesmo quase todos os dias. Levantava muito cedo, ia ao coqueiral e juntava uma dezena de cocos e pouco a pouco, de coco em coco, dinheiro nos meus bolsos já tinha um pouco.
Sem me preocupar tanto nem andar buscando, o dinheiro sozinho foi chegando. Fui conhecer a ilha iguana que tinha que chegar em lancha e pagar pela viagem, me alimentei muito bem e aprendi essa grande lição.
Quando saí da praia de Pedasi levava uns trinta dólares e suficiente comida como para dois dias. Descobri que quando fiquei sem nenhum centavo não tive necessidade de desesperação, só tive que confiar que de alguma forma ou outra o dinheiro chega.
Essa experiência me ensinou a ir perdendo o medo de ficar sem dinheiro e a descobrir que tem milhares de formas para gerá–lo.
Ao perder o medo a isso me fazia sentir que começava a viajar de um modo mais livre.
E como você sabe, com a liberdade vem a felicidade.
No seio da Índia
Nua, quieta, deitada no seu leito, bela, profunda, dormida. Inerte, muito serena.
Dava a impressão que podia estar sem vida, mas assim toda ela era linda.
Cada curva de seu corpo encaixava perfeitamente um com o outro, não mostrava defeito algum.
Os pés, os joelhos, o tronco, suas mão, sua cabeça... seu rosto, estava coberta de um resplandor intenso e reluzente, como a estátua de uma virgem esculpida pelos deuses gregos.
Uma mulher, uma dama, uma anciã, uma menina, uma escrava, uma deusa, uma esfinge, uma silhueta, uma imaginação.
Uma ÍNDIA.
De perto ou de muito longe podia ver–la eternamente submergida em seu profundo e infinito sonho.
Eu a vi e me namorei dela com toda minha alma, quis sentir seu calor, o bater do seu coração, escutar sua respiração, inalar seu cheiro, me banhar na fonte do seu existir, beber do seu ser, ficar ao seu lado e amar–la para sempre. Meu desejo total foi dormir com ela.
Sem intenção alguma de incomodar e com o mais dos respeitos requeridos, me aproximei dela e comecei a contemplar, deixando entrar em minha consciência sua imagem celestial. Logo, convencido de não quebrá–la, me animei a tocar–la, a sentir–la.
Comecei por seus pés, me introduzindo na sua grandeza. De um lado dos seus tornozelos encontrei a trilha que me guiou pelas suas pernas até chegar ao seu ventre, rodeando seu umbigo, caminhei rumo ao leste e escalando pelas suas costelas consegui subir até seu seio esquerdo, terminando exausto, rendido, cansado entretanto satisfeito encostei no seu mamilos. Fiquei aí e lhe disse:
– Bela mulher, meu amor. – Olhando fixamente os seus olhos fechados supliquei–. Permita–me ficar para sempre ao teu lado, porque você é vida, porque você é paz, porque é casa, é lar, é confiança, esperança e felicidade... é amor.
Todos te admiram e logo se vão, te ferem, fazem chacota, te julgam, te ignoram, te vendem, te retratam e te matam.
Permita–me por favor ficar aqui para te oferecer o melhor de mim, me conta sua história, me dá tua verdade, me comparte tua vida, me enche da sua bondade.
Faça-me  parte de ti e não desconfie de mim, minhas mãos não te farão danos, deixa–me mostrar que sou diferente dos outros humanos e quando passe o tempo e ainda que não te despertes jamais, seguirei vivendo aqui, de ti, junto a ti.
Deixa–me tocar tua mão, deixa–me demostrar com atos o muito que te amo.
Logo um leve sopro saiu da sua boca chegando até mim como uma brisa, secando o suor e as lágrimas. Nesse gesto de êxtase e sublime, senti que desde suas entranhas ela, A Índia Adormecida, me disse que sim.

Uma novela é uma família
A cena começa com uma imagem de um sujeito sentado dentro de um carro estacionado no estacionamento de um centro comercial.
Tem como escutar a música instrumental de fundo com um ritmo de suspense.
Lê as mensagens de texto no seu celular, está pasmado como desmoronado com a informação.
Nunca imaginou reviver e justo nesses momentos uma notícia assim. O aspecto o seu rosto é de raiva, tragédia e parece que queria chorar.
Na seguinte cena, aparece o mesmo tipo. Dessa vez está de pé junto a uma maca onde esta deitada uma mulher de uns cinquenta anos com uma leve sorriso.
Do outro lado da maca, um ansiado médico de roupa branca e um estetoscópio pendurado no seu pescoço.
Os três observam uma pequena tela onde sai um cabo que dá numa aparato que o médico pressiona sobre o estômago da mulher.
Médico: – Sim, efetivamente o caso apresenta um normal estado de gestação, querem saber o que é?
Ela: Sim, doutor.
Ele: Está bem.
Médico: Bem, vamos ver melhor – gira um botão do aparato e continua pressionando o estômago por vários lados–. Tá, Tá, esperem, o que é isso? Ah! Mas olhem que surpresa! São duas criaturas.
Ele: – Ah?
Ela: – Nãooooo!
Médico: – Um segundo, um segundo, que?... hahahaha, são três.
Ele: – Ah? Não pode ser.
Ela: – Santo Deus!
Médico: – Felicidades! Serão pais de trigêmeos, não se preocupem, eu me encarregarei do parto.
O dia que senhor Torres me contou sua história, pensei que estava brincando comigo ou que melhor, que tinha tirado de uma novela mexicana. Mas era uma história real e me continuou contando.
– Olha Chepe, quando eu a conheci, ela já tinha filhos grandes e além, ela já havia se esterilizado, mas isso não importou para mim porque a queria muito, tudo ia muito bem com meu trabalho, fomos crescendo economicamente e investindo alguns lucros na compra de propriedades. Mas com o tempo, ela foi se tornando muito ciumenta, eu estava muito ocupado nos negócios e a confiança em mim ela foi perdendo e isso também fez que o amor fosse se esfriando.
Depois se meteu a ideia que queria ter um filho comigo. Eu creio que um ato assim era para me capturar ou chantagear. Eu lhe disse que não.
Uma noite me conta que havia vendido um apartamento que tínhamos no seu nome, mas que claro, eu o havia pagado, e essa ação me encomendou muito por não me consultar.
O grave era que com o dinheiro pagou um dinheirão ao médico que prometeu que ela voltaria a ser fértil. Aquilo foi uma loucura, então decidi me mudar para esse povoado e comecei esse negócio do açougue e quando lhe disse que queria me divorciar, me fez um grande escândalo, quase me mata.
Mas ao fim, com o tempo, aceitou o divórcio e me contou suas condições. Queria uma, que passasse os outros bens ao seu nome e que lhe desse uma boa amostra do meu esperma. Hahaha, sim que estava louca, isso já era uma obsessão, mas como prometeu me deixar tranquilo, ao fim aceitei.
Aqui conheci a Luz, minha atual esposa, namoramos e quando consegui o divórcio nos casamos e aos poucos meses ficou grávida, nós dois nos sentimos muito felizes. Mas não vai acreditar que no mês que Luz ficou grávida, minha ex–esposa me mandou uma mensagem enquanto eu saia de um centro comercial. Dizendo que havia conseguido depois de andar de um laboratório ao outro, estava grávida, mas que havia gastado todo o dinheiro em médicos e que queria que eu arcasse com todos os gastos. Tudo pelo seu capricho.
Quando contei isso a Luz, minha esposa, quase me deixou, sofreu uma depressão e sua gravidez estava de muito risco. E mais quando nos soubemos que eram trigêmeos. Eu queria sair daqui e desaparecer, olha, talvez se tivesse passado por aqui nesses dias te asseguro que agarraria uma bicicleta e iria contigo.
O médico nos explicou que como, claro, foi tanto a desordem de tratamentos que ela teve que quando seu óvulo veio a receber um, entraram três.
Bem, e aos meses, num mesmo ano, eu era pai de quatro filhos: três meninos e uma menina.
A lei desse país me exige pagar a pensão para a criação dos trigêmeos e por isso você vê que trabalho até os domingos. Agradeço a minha esposa que não me deixou nesses momentos complicados, sei que me ama, me tem demonstrado, eu a amo muito também. Ela colocou um negocinho e aí vamos passando, vamos nos ajudando, passando juntos as circunstâncias.
Os trigêmeos já estão grandes, têm nove anos igual que minha filhinha, trago eles nas férias e quando estão aqui jogam os quatro juntos e vejo que minha esposa de algum modo também os quer. Claro que eu sim os quero muito aos quatro, são meus filhos.
Minha vida e minha história parece um capítulo de uma novela Chepe, mas é real.
Logo de me contar isso, pegou duas xícaras de café, foi até a cozinha, preparou, voltou e me disse:
– Melhor tomemos café.
Seu Francisco e sua família foram as pessoas que me ajudaram a continuar minha viagem.
Quando eu cheguei ao povoado de El Valle del Antón, passei uma semana dormindo em um parque depois que todas as crianças deixavam de jogar, comia frutas e verduras que recuperava da lixeira do mercado municipal. Mas quando o conheci, ele me deu trabalho, me deu boa comida e me colocou em um quarto da casa dos seus sogros, ao pé do morro da Índia Adormecida, também compartimos de muitos momentos de risadas, muitas risadas…
Eles são minha família, eles são esses tipos de anjos que te aparecem no caminho e os quero com toda a minha alma.
Uma noite que não podia dormir por forte dor de estômago, eles me levaram ao hospital, me cuidaram quando estive doente, estiveram do meu lado e choraram no dia que nos despedimos.
Luz, seu Alcides, a “madrecita25” Saturnina e seu Francisco Torres. Sempre estarão nas minhas mais queridas memórias.
Quando cheguei a esse povoado, não tinha muito dinheiro em pouco tempo sai com uma quantidade suficiente para pagar um barco que me levaria a Colômbia. Tinha boa saúde, a Delgadina estava em muitas boas condições e, o melhor, que conheci uma nova família que me dizia que voltasse logo, que seus braços sempre estarão abertos para o meu regresso.

Hoje não tem estrada
A agitação dos carros e o medo da imprudência dos motoristas, faziam que meu estômago doesse pelo nó de nervos acumulados.
A noite anterior não havia passado da estação dos bombeiros de Fraijanes, na grande cidade. Fui convidado a jantar com todos os oficiais aí presentes e meu amigo José Vega, que foi quem conheci. Meu objetivo principal aquele dia era conseguir sair ileso do trânsito da cidade e chegar o mais próximo possível do Tapón del Darién.
Conduzir na bicicleta pelos primeiros vinte quilômetros e poder escapar do caos foi o mais parecido a uma proeza ou a uma missão suicida, sério. Os ônibus passavam muito perto, quase roçando, as motos e os táxis passavam acelerando a toda velocidade para chegar a tempo do outro semáforo verde e alguns me buzinavam para que eu desviasse ou saísse da estrada mesmo que eu já estava indo muito perto da beirada.
Era muito impossível sair tranquilo frente a pressa que todo mundo levava, seus trabalhos, seus negócios, suas vidas urbanas. Era como um ato de pressa diária de meses e de anos até chegar a suas próprias mortes.
Quando comecei a ver os campos e as casas mais dispersas, parei a tomar um pouco de água, voltei para olhar para trás e me senti melhor, com a imagem apenas distinguida dos prédios.
Lá pelas quatro da tarde cheguei ao desvio de El Llano, dali continuaria pela estrada dentro de um vasto bosque tropical primário de quarenta quilômetros até chegar ao porto onde nos embarcaríamos com a Delgadina para navegar pelo mar do Caribe e entrar a Colômbia pelo porto Turbo.
É dessa forma ou por avião que se pode chegar ao território sul-americano, porque já não tem caminho confiável, a estrada termina aí em Tapón del Darién.
A rua até o desvio tinha a vantagem de ser plana, em bom estado e sem interrupções. Entretanto, o sol ardia tão forte que esquentava a água para beber que eu trazia nas garrafas.
Os quarenta quilômetros que era o outro trecho restante para chegar ao porto, segui em puras subidas e descidas, porque foi construída sem cortar os morros para causar o menor dano possível ao bosque, que era também uma reserva natural do Estado. Continuei entrando ao bosque, consegui avançar uns quinze quilômetros e aí encontrei uma velha pousada que estava semiabandonada, já dentro da região dos nativos Guna Yala.
Chegue de noite, escorrendo água pela chuva que estava caindo desde o entardecer.
A proprietária do lugar, uma europeia, me deixou armar a barraca debaixo de um tetinho por cinco dólares e com café da manhã incluso.
O outro dia, saí cedo e durante o trajeto ia observando a variedade de árvores gigantes que sobressaiam entre as samambaias e arbustos, bananeiras de diferentes tipos, coqueiros avós, uma variedade impressionante de flores e orquídeas que nunca antes havia visto.
Cheguei a um pedágio cuidado pelos nativos, onde tive com eles uma conversa muito amena na qual contei que eu também era descendente de uma etnia ancestral americana, mas apesar da simpatia que mostraram para mim, sempre me cobraram os vinte dólares exigido para passar dali para lá. Foi evidente que eu estava introduzindo um ecossistema próprio do caribe. Uma quantidade de sapinhos que olhava na beira da rua e lagartixas de cores variadas e intensas, todos anfíbios bonitos, exóticos e únicos da zona. Ao meio dia, a umidade do lugar era muito forte, quase insuportável, tomei toda a água que restava e segui confiando encher elas em algum rio no caminho.
Empurrando a bicicleta consegui subir uma subida bastante íngreme e já lá em cima, encostei a bicicleta em uma árvore muito grossa e deitei na erva com o peito muito agitado. Tratei de me tranquilizar e de respirar mais devagar, mais fundo para diminuir o aceleramento do coração e logo senti um grande aroma de flores, não sei de quais mas o aroma era tão agradável…
Não podia crer! Estava numa selva, vivendo cada instante em um estado subjetivo e ali deitado, desde minha perspectiva, comecei a contemplar a bicicleta e pensei: que instrumento mágico é esse? Que me transporta a lugares bonitos e maravilhosos, com suas rodas girando como o mundo, em contínuos ciclos infinitos. Para fazê–la funcionar só necessita o esforço constante mas que as vezes é gratificante para o corpo, é assim que se ativa todo e o sangue chega até os mais escondidos cantos das veias e sobe até a sua cabeça, banhando os neurônios. O movimento contínuo nela te lava a um equilíbrio na lei gravitacional que dá uma sensação de levitar.
Sobre ela, toda percepção do redor é de primeira mão, transportando assim a nossa consciência a uma dimensão abertamente sensitiva, alimenta o nosso ser e o exercício com os pés chega a uma sincronização com o avançar, e seu girar forma parte de uma engrenagem coordenada com o complexo funcionar do nosso planeta, na via láctea, no universo, no cosmos. E suspeitando esses enigmas quero entender que a bicicleta e um mistério a mais, é uma fábrica de experiências, é um infinito de possibilidades e é um dos melhores meios de meditação. A bicicleta é mais que metais e borracha.
Logo, terminando meu monólogo e com as forças renovadas, me levantei, peguei a bicicleta e disse a ela: maravilhosa Delgadina, me leva onde você queira.
E me deixei ir a toda velocidade nas descidas com os olhos fechados, percebendo somente os sons do bosque, cheirando os aromas e sentindo a brisa fria no meu rosto. Logo, quando abri os olhos calculando chegar, já era tarde, não pude me desviar e me choquei contra um arbusto e eu segui girando umas duas ou três voltas mais. Fiquei aí deitado uns minutos rindo e revisando si não havia quebrado uma costela, mas estava bem. Logo me deu muita vontade de mijar e me levantei. Fui até uma pedra e mijei, mas percebi que de um lado, a beira da rua, tinha um montinho de cubinhos de gelo e latas de cervejas dispersas e vazias, pensei: passou por aqui um turista que vinha da praia com seu carro e deixou isso aqui, certo voltava de uma manhã de pesca, esvaziou a caixa térmica aqui e se foi.
Voltei até a bicicleta e peguei as garrafas rápido e voltei para encher com os pedacinhos de gelo e isso serviria de água gelada. Voltei co montinho de cubinhos de gelo, separei as latas vazias de cerveja que estavam sobre o gelo e entre elas, havia uma cheia... bem fria.


O mais importante
Se você gosta de se afastar da sua casa, do seu conforto, dos seus vizinhos, do seu círculo social de amigos, da sua cidade.
Se tem em você uma simpatia natural por ir mais lá dos seus limites e expectativas.
Se te surpreende ver uma cachoeira, um entardecer ou um arco–íris e encontra a beleza nos detalhes mais pequenos.
Se te encanta se banhar nos rios, no mar, nos lagos ou com a chuva.
Se te fascina subir uma grande subida fazendo piada, fazendo apostas ou cantando a gritos.
Se valoriza um pedaço de pão, uma fruta, um copo de água ou um gesto amável.
Se te aborrece a agitação das pessoas e dos carros e busca refúgio na calmaria de um bosque, no som dos animais e nas folhas movidas pelo vento se liberando assim dos seus apegos, dos seus hábitos e aprendendo a esquecer os nomes dos dias.
Se busca uma maneira de te conhecer, te transformar e encontrar a si mesmo, sem medo da solidão, da escuridão, a ficar sem dinheiro, sem medo de dormir debaixo de uma ponte, em um cemitério ou onde seja.
Sem pânico de morrer.
Sem medo no ouvido.
Então pode ser que já esteja preparado para uma larga viagem em bicicleta.


Uma casa para todos
Enquanto armava a bicicleta no porto de Turbo, escutava longe uma canção tristona. Nisso estava quando um policial se aproximou e me comprimento.
– Amigo – lhe disse – como se chama essa canção? – ele ficou quieto escutando com atenção e me respondeu.
– Ah! Sim, é um vallenato26 dos velhos, a música se chama Sirena encantada.
– Muito obrigada amigo – respondi e comecei a cantarolar a canção enquanto apertava os parafusos das rodas da Delgadina.
No caminho da navegação tive que tomar três embarcações para chegar ao porto de Turbo, Colômbia. A primeira foi uma lancha desde porto Cartí no Panamá, na qual demoramos umas seis horas para chegar ao porto Obaldía onde está o limite entre os dois países e onde estão os escritórios de imigração. Logo outra lancha me levou ao porto de Capurganá, onde passei dois dias tranquilos com entardeceres de filme. De aí tive que embarcar em um catamarã ou iate que nos levou a Turbo.
Todo aquele percurso por águas dessas costas caribenhas, foi uma experiência visual belíssima, com mais de trezentas pequenas ilhas onde muitos dos nativos passam suas vidas inteiras junto a suas famílias. Os vi contentes, as mulheres muito seguras e cômodas com suas identidades e as crianças corredores e travessos que me deram uma imagem tão bonita que não esquecerei jamais, ficou na minha memória, los ver uma tarde nadando muito perto das suas casas jogando com golfinhos.
Finalizando minha tarefa e já com as mochilas bem amarradas na bicicleta, sai buscando o policial que me havia cumprimentado. O encontrei sentado, apoiado em um dos postes de lâmpadas do píer, escutando a mesma canção no seu celular.
Dessa vez o perguntei por uma bicicletaria, por um lugar para dormir e onde poderia conseguir sinal de internet.
Dando-me  todas essas informações, saí do porto à bicicletaria. Era uma bicicletaria muito completa e sem complicações. Alinharam as rodas, todos que trabalhavam eram uma só família de afrodescendentes e quem administrava era a esposa do dono. Foram muito amáveis e me fizeram o trabalho sem nenhum custo. Logo saí disparado e contente a estação dos bombeiros que não tive necessidade de lhes dar muita explicação para que me deixassem dormir essa noite e também me deram a chave da internet do escritório. Assim, pela noite, deitadinho dentro da minha barraca, conversei com a minha mãe e com Fabiola que já estava em Colômbia. De repente chega uma mensagem de Matías Daniele, um viajante de bicicleta com o qual havia cruzado em Costa Rica.
A mensagem dizia: Hei! Chepe, te mando a direção e o contato da casa ciclista de Medellín, no deixe de passar por aí.
Logo de tomar nota de tudo isso, descarreguei a música que escutei no porto e dormi.
Na manhã, depois de um bom banho e um café, preparei todos meus equipamentos de viagem, fui agradecer os bombeiros, tirei uma foto e sai com otimismo; comecei a pedalar quatrocentos quilômetros mais ou menos até a cidade de Medellín, cantando.
”Y tú llegaste, cuando yo más te necesitabaaa... señalando, el camino menos doloroso...”27
O mundo debaixo de um teto
Se a vida te dá um presente de viajar em bicicleta pelas ruas de Colômbia, a rota do sol, a costa caribenha ou pela zona de Antioquia até chegar a Santa Fé.
Não vai tão rápido nem com pressa, viaja tranquilo conhecendo e desfrutando cada região, cada povoado e contemplando cada paisagem. Passará por plantações de bananas, arroz, milharais, feijão, mandioca, etc.
Seu clima tropical é merecedor de aproveita com bom espírito e ânimo. Pode parrar e descansar nas suas praias e conversar com os avós e as “mamitas28”, jogar com as crianças e se namorar de uma bela mulher, se fazer irmão de um desconhecido... Os “paisas29”, como dizem, são alegres, tranquilos e trabalhadores.
Da minha parte não tenho reclamações, quando passei por essa linda região da Colômbia me dava a sensação de viver em um dos contes de Gabriel Garcia Márquez.
Alojei-me uns dias na cidade colonial de Santa Fe, que em outro tempo foi a capital do país, caminhei o mais que pude pelos seus redores, pelas suas ruas empedradas e admirando suas velhas casas de paredes de adobe e tetos de telha de barro. Sua igreja de 1800 foi então testemunha de muitos acontecimentos históricos antigos.
De pouquinho fui deslizando em seu encanto, me sentindo como um mais de eles, de los que chegam a Colômbia com medo e discrepância, mas que no terceiro dia vão gostando de todo aquele ambiente e não querem ir–se. Fui chegando a grande cidade e quando vi já estava passando debaixo do letreiro gigante que dizia: “Bem-vindos a Medellín”.
Milhões de pessoas movendo–se nos seus transportes nas rodovias que se uniam a uma rede muito complexa de ruas, rotatórias e avenidas. A pobre gente ia de um lado a outro apertados nos ônibus e nos teleféricos que passavam por cima de mim pendurados nos cabos até lá em cima dos cerros, aquele cartão postal era como ver um formigueiro gigante, era a cidade mais urbana que havia conhecido na minha vida e o pior que eu estava chegando em bicicleta.
Perguntando, cheguei ao centro da cidade, cansado e nervoso, me ocorreu de parar em uma esquina onde estavam vários taxistas, voltei a ler o papelzinho do endereço e me aproximei de um dos taxistas para que me desse alguma orientação.
– Sim, você segue essa avenida direto, são uns sete quilômetros até São Antonio do Prado, ao chegar na praça pergunte pela direção dessa casa, vai tranquilo parceiro – disse. Obrigada parceiro – o respondi.
Mas não foi tão fácil chegar, não pelos carros nem porque me perdi, mais porque todo o caminho era pura subida até chegar a praça.
Continuando com o método de PPS (pare, pergunte e siga) me aproximei de uns meninos que andavam de bicicleta e perguntei onde ficava a casa ciclista e com muita cortesia, eles me levaram ao lugar.
– Aqui é parceiro, você entra aí e bem, alguém o vai atender, tchau parceiro.
Mas era uma bicicletaria e espécie de loja com artigos de ciclismo e cicloturismo, entrei e cumprimentei a jovem que estava atrás do balcão.
– Oi, boa tarde, busco seu Manuel Velázquez.
– Qual é o motivo? – Perguntou.
– Um amigo me deu esse endereço e me disse que era uma casa onde se hospedavam os que viajam em bicicleta.
Sim, espere um momento, chamo a Martha, a esposa – depois de uns minutos chegou ela.
– Oi, sou Martha, esposa de Manuel. Olha, ele não está aqui mas te darei o endereço da casa ciclista. É aqui perto a uns três quilômetros, tem algumas subidas no caminho, mas essa área é mais de campo, simplesmente vá.
A casa estava sobre um terreno inclinado, com árvores e muito jardim. Ao entrar um cão grande saiu a me receber movendo seu rabo e rapidamente outro cão, menor, correu até mim latindo muito histérico e querendo me morder.
– Hei! Tranquilo, tranquilo, passe, passe, pois seja bem-vindo. Você, de onde é?
Era seu Manuel que estava com uma apá na mão. Eric! – gritou. E apareceu um jovem que se apresentou como Eric, que me disse que era venezuelano e que estava de voluntário na casa ajudando no jardim e recebendo os cicloviageiros. Demos uma breve volta pela casa, me indicou as normas e disciplinas e me explicou como funcionava o projeto. Logo me reservou uma cama onde poderia dormir e ao final, me pediu meu passaporte para anotar meus dados.
Depois fomos nos apresentar aos outros viageiros que estavam alojados na casa e estavam conversando em outra área do lugar.
Um casal de franceses já mais velhos, um alemão e uma jovem de Buenos Aires, Argentina.
Na noite, recém terminando de jantar, chegaram seu Manuel e Martha, tomando cafézinho juntos e nos compartiram um pouco da história de como começou o projeto da casa ciclista, a qual me pareceu uma história muito bonita.
– Por mais de quinze anos Martha e eu temos recebido aqui muitos cicloviageiros de todo o mundo, mas nunca havia chegado um salvadorenho, te damos as boas vindas.
Ao início os recebíamos na nossa própria casa, mas com os anos foram chegando mais e mais cada dia, por isso, com a ajuda de alguns deles, fomos construindo essa casa que é somente para vocês, aqui a tem. Está feita com muito amor, aqui podem sentir–se em como na sua própria casa, cuidem e mantenham–a sempre limpa e que este lugar siga com a boa energia de sempre.
Em nossa família valoramos todo o esforço que vocês fazem para conquistar seus sonhos de viajar e conhecer outros lugares usando a bicicleta, nós também sabemos a liberdade que se vive quando se viaja de bicicleta e o feliz que se sente chegar a um lugar onde se pode descansar, tomar banho, lavar a roupa, reparar a bicicleta, cozinhar e comer o que na estrada é difícil.
Com essa consciência humana estamos aqui, recebendo todo aquele que necessite deste lar mas que venha com boa vontade.
Da parte de Martha e da minha, é uma honra os alojar e quando se vão e terminem sua viagem, lembrem que a casa do ciclista de Medellín os esperará com os braços abertos.
Depois destas palavras, espontaneamente todos ficamos de pé e aplaudimos. Depois, cada um de nós foi até eles, lhes agradecemos e os abraçamos.
Hoje o chinês cozinha
Querida Fabiola, espero que esteja estável de saúde e que se sinta bem com seus amigos no instituto.
Não posso crer quão rápido passou esses seis meses e o quanto eu sinto sua falta a cada dia que passa e hoje, mais ainda, por ser seu aniversário.
Sobre a minha situação, pois te digo que me sinto bem, sigo fazendo trabalhos de soldagem aqui na casa do ciclista de Medellín. Seu Manuel e dona Martha são pessoas agradáveis e carinhosas, quase sempre almoçamos juntos e a janta fazemos em coletivo com os outros viajantes que ficam uns dias na casa. Desde que cheguei aqui, essa casa nunca ficou sozinha e além disso vou conhecendo ciclistas de muitos países.
A semana passada trouxeram um pobre venezuelano que ia na sua bicicleta rumo a Peru na busca de trabalho e enquanto vinha para cá, em uma das curvas, não conseguiu frear e foi chocar–se contra um poste de luz. Alguém o levou ao hospital e logo dona Martha foi até lá para trazê–lo na sua caminhonete.
Quando o trouxeram vinha com a cara e as mãos toda raspada. Enquanto estava deitado no sofá grande da sala, fomos todos a comprimentá-lo e nos apresentar, seu Manuel perguntou se doía muito a inflamação da testa, mas o jovem venezuelano disse que não tinha batido a testa e que era assim, testudo de nascimento. O francês e o uruguaio não puderam disfarçar e soltaram risadas, ao final todos rimos até mesmo o acidentado, somente dona Martha riu um pouquinho disfarçadamente.
O cara está cara dia melhor, o seguinte dia chegou Enio Paipa, um brasileiro gordinho, seu Manuel disse que era o primeiro cicloviageiro gordinho que chegava a casa, agora somos bons amigos e penso e acredito que vou viajar com ele um tempo, já que ele tem o mesmo caminho até Argentina.
Eric, o outro venezuelano que estava de voluntário, se foi e seu manuel me pediu que fizesse cargo de receber os ciclistas que cheguem, anotar seus dados, lhes dar seu espaço e indicar como funciona a casa.
Faz três dias que chegou um chinês, era quase de noite, eu estava pintando uma janela e apesar do barulho do compressor, consegui escutar as batidas que dava no portão. Desliguei o compressor e fui rápido, quando perguntei quem era, me gritou:
– Casa ciclista? Casa ciclista? Tenho uma emergência!
Abri o portão rápido para ele e entrou com sua bicicleta, que quando a vi pensei que a traia muito carregada. De repente o cachorrinho saiu correndo da sua casa onde dorme e logo chegou até ele latindo muito eufórico, fiquei na frente dele não o deixei passar. O chinês me olhava e dizia:
– Emergência, emergência! – e atirava chutes ao cachorrinho. Numa dessas o animal ficou nervoso e lhe mordeu um pé e ficando travado com os dentes na sua meia, eu o toquei com a vassoura e o afastei para que o pobre chinês terminasse de entrar na casa, ele jogou sua bicicleta e me disse:
– Banheiro, banheiro... por favor, quero cagar.
Deixamos jogada a bicicleta aí e lhe disse para que me seguisse para lhe guiar onde estava o banheiro e o levei, sempre cuidando que o cachorrinho não o mordesse de novo. Abri a porta e entrou baixando a bermuda, fechei a porta e não me passou outra coisa na cabeça que desejar que ele tivesse uma boa cagada. Se chama Silvio e tem cinco anos de percorrer o mundo na sua bicicleta, a verdade é um chinês muito simpático, nos faz rir todo o tempo com a sua forma de falar e tem estilo, te prometo, aqui o queremos muito.
Ontem pela manhã, o cachorrinho entrou na casa e subiu até onde dorme o chinês e pegou uma das suas cuecas. Nós estávamos tomando café na sala e o cachorrinho parrou na nossa frente como um ladrão inexperiente.
O animal, como que já tem o Silvio bem chinês de tanto zoar, as vezes chega a latir para ele enquanto está deitado na rede ou escrevendo no seu computador sentado na sala. Também acostumou a pegar os papéis usados do banheiro e se deixa um pão na mesa, ele chega e sobe pela cadeira e o leva, todos temos que estar sempre de olho nele.
Julián, o francês, propôs ir caminhar por la tarde nuns morros bem bonitos que tem perto daqui. Seu Manuel se juntou na ideia, Enio, o brasileiro, também e eu claro que sim, já sabe, que eu gosto dessas coisas. Na casa ficou um casal de colombianos que estão fabricando seus alforjes de garrafões de plástico, o venezuelano com a cara arrebentada e o chinês que não quis vir com a gente, disse que ele cozinharia essa noite o jantar para todos.
Posse dizer que aproveitei da caminhada, do ambiente de campo e das histórias contadas por seu Manuel perto do lugar. Já era de noite quando voltamos e encontramos o chinês na cozinha muito inspirado, picando verduras, adicionando tempeiros, abría uma panela e tampava outra, parecia como se estivera dirigindo uma orquestra sinfônica. Em pouco tempo saiu batendo uma frigideira com uma colher, chamou a todos à mesa e começou a servir pratinhos com arroz e pratos fundos com sopa. Ao centro da mesa colocou uma vasilha grande com salada e um prato com um vulcão de pães de alho.
Enio subiu para trazer o venezuelano que passava sua recuperação no quarto de cima, os colombianos foram buscar umas cervejas e Julián colocou na mesa uma garrafa de vinho. Seu Manuel foi chamar a dona Martha e depois todos juntos reunidos começamos a comer.
Percebi que a sopa tinha uns pedacinhos de carne, assim quem peguei uma porção extra de arroz, salada e pão de alho.
Tinha comida suficiente para todos, Silvio repetia constantemente que deveríamos repetir um prato a mais, o convalescente pediu o favor que enchesse seu prato com mais sopa enquanto o brasileiro já ia tomando como três, Julián nos fazia rir com seu sotaque francês e o cara colombiano nos prometeu cantar um hip hop depois de jantar.
– De que é a sopa Silvio? – perguntou dona Martha.
– Flango – respondeu rapidamente o chinês.
Mas, porque essa carne está tão durinha e escura? – perguntou o da cara arrebentada.
– Temperos, molho de soja e secredos orientais – voltou a responder muito rápido o chinês.
Dona Marta parou de tomar a sopa na hora, olhou a seu Manuel e disse.
– Popi! Meu cachorrinho, não o vejo desde o meio–dia.
Repentinamente todos deixaram de tomar sopa, seu Manuel olhou fixamente o chinês e exclamou:
– Ei! Chinês, não me diga que fez ensopado de cachorro?
– Ai! – que queixou dona Martha – me doe a barriga, quero vomitar e saiu correndo ao banheiro.
– Chinês terrível! – comentou o venezuelano com a cara raspada.
– Umm, mas tá saborosa – opinou Enio o brasileiro.
E eu não pude me conter mais e comecei a rir, me esforçando em não fazer isso.
O chinês trocou de cor, seu rosto amarelo se tornou vermelhinho na nossa frente que o estávamos olhando, abriu sua boca fazendo para a gente um grande sorriso nervoso, nos mostrando todos seus dentes e seus olhinhos ficaram mais pequenos a ponto de fechar.
– Não é cacholinho, eu plometo – dizia muito assustado.
– É flango, seu Manuel, é flango... se eu mato cacholinho, seu Manuel faz chichalon30 de mim.
E aí foi que todos não podíamos conter mais a risada e nos rimos até chegar as gargalhadas.
– Mas tem gosto de cachorro – disse o francês –. Eu estive no Vietnã e comi cachorro.
Os colombianos separaram seus pratos de sopa e se afastaram deles, Enio entretanto se levantou e foi por mais.
Depois do jantar e lavar os pratos, nos começamos a conversar e tomar a cerveja, nunca vou esquecer estes momentos que vivi aqui com meus amigos e com a família dessa casa.
Tranquila filha, Popi, o cachorrinho, apareceu no dia seguinte.
Chegou bem sujo e maltratado, suspeitamos que esteve namorando.
Feliz aniversário.
10 de julho de 2018 Medellín, Colômbia

Tem dias assim
Tem dias assim.
Dias de chuva e logo fura a roda da bicicleta.
Os freio já estavam muito gastos e tem que meter os sapatos nas descidas, também os sapatos já estão muito gastos e rasgados.
A corrente não dá mais, já não pode tirar outro elo, a praia a deixou enferrujada e não tem mais graxa.
Esse ruído na roda de trás, só espero que não seja o eixo, podia ser caro.
O assento vai rasgando, já são duas vezes, o maldito parafuso que não prende e os pedais piam como um franguinho. Por que coloquei os mais baratos?
Essa garupa é uma merda, já tá bamba e entra água nos alforjes, o açúcar se molhou, o gás acaba rápido, uma dor no joelho esquerdo e a bunda que arde os dez primeiros quilômetros.
Os motoristas perversos que te buzinam para te assustar, onde tá as ruas planas? Engenheiros de estradas que não cortam os morros e muito cuidado com os cachorros que podem te morder.
A roupa pálida de tanto sol, intempéries e sal de suor, hoje é o quarto dia que não tem banho e as pessoas te ignoram, já não te cumprimentam.
A polícia que pega as suas coisas e revisa tudo buscando maconha e te enche o saco e logo se pergunta: a verdade, não sei porque fiz essa estúpida viagem? Talvez foi somente porque me emocionei.
Nada compra seus artesanatos e o malabarismo no semáforo já não dá dinheiro.
O frio, o vento, o sol, a fome, o medo, a loucura, o esquecimento... a noite.
E outra vez não encontra onde dormir.


Chapéis, café e cachaça
A manhã que Enio e eu saímos da casa do ciclista de Medellín, era todo um ambiente de sentimentos encontrado, alegria de ter conhecido a essa gente tão cheia de luz e ter vivido juntos tantos momentos de alegria, afeto e conhecimentos compartilhados. Para mim foi toda uma experiência que agrega à minha vida, mas o tempo de continuar com a viagem era o propício. E nos abraçamos todos depois da típica foto no jardim com nossas bicicletas, o chinês tirou algumas da gente quando estávamos indo e seu Manuel e o venezuelano se despediram da gente desde o portão da entrada. O francês nos acompanhou até o povoado e daí começamos a descer até uma saída da grande cidade rumo a duas divisões, em Pereira onde José, com quem já havíamos nos havia comunicado antecipadamente, nos receberia em uma casa ocupa que também funcionava como universidade alternativa.
No segundo dia de viagem com o meu companheiro brasileiro, chegamos à Dosquebradas, José nos escreveu numa mensagem que não estava nesse momento aí mas que os meninos estavam esperando a gente na casa. Quando ao fim de tanto buscar a direção encontramos o lugar, vi a Luci, a Argentina que conheci na casa ciclista, estava na sala da casa e ao ver–la a gritei desde fora, ela saiu correndo e nos deu as boas vindas. Aí lhe apresentei Enio e logo entramos com as bicicletas. A semana que passamos nessa casa foi outra grande experiência de todas as cores, tínhamos conversas sobre geopolítica todas as manhãs enquanto tomávamos café, ajudando um pouquinho com os projetos de bioconstrução e cada dia adquiríamos novos conhecimentos na tecelagem com técnicas ancestrais e receitas culinárias próprias da região.
Lúcia, Enio e eu fomos convidados a dar uma pequena apresentação da economia política e realidade conjuntural dos nossos países a um comité de base de uns membros de um movimento campesino. A última noite, as meninas e meninos que moravam na casa ocupa celebraram uma festa de aniversário a uma das jovens mais conhecidas e professoras da universidade.
Tivemos um momento de meditação, um círculo de palavras, comemos o jantar especial e a aniversáriante cortou o bolo e logo arrancou a festa com puras cumbias e uns de outros ritmos tropicais. Enio tomou vinho, cerveja, guarapo e por último creio que andava um xarope para tosse na sua mão e dançamos até quase o amanhecer.
Saímos rumo a Manizales. Enio tinha falado com Pilar, uma jovem que hospedava cicloviageiros desde já vários anos e nos recomendou chegar a Manizales não pela estrada principal mas sim or uma rota alternativa que passaria por uns povoadinhos, percorrendo uma boa parte da cordilheira andina.
Uma manhã que estávamos fazendo o café, enquanto fui lavar os pratos, um cachorro levou o meio quilo de queijo que acabávamos de comprar, apenas consegui ver e comecei a gritar a Enio que o seguisse e Enio começou a seguir o cachorro. E essa perseguição também se juntou um senhor que estava passando por aí e eu fui atrás deles, o cachorro cruzou a rua e quando chegou ao outro lado, um cara de moto o enfrentou e desse modo o cachorro soltou o queijo. O senhor chegou primeiro, pegou o queijo e a entregou a Enio quem a recebeu gargalhando do divertido que foi.
Decidimos considerar a recomendação de Pilar de irmos pelas montanhas e iniciamos o caminho designando como meta do dia chegar ao povoado de Aguadas, que era muito mencionado por ser o maior produtor de chapéus do país, inclusive o presidente de Colômbia usava um chapéu feito em Aguadas.
Mas os 50 quilômetros para chegar não foram tão fáceis de realiza. Pela primeira vez, olhei a Enio que tremia todo o corpo e a voz se ia do cansaço que provocada as subidas íngremes.
O vivido neste povoado foi muito bonito, o escritório de turismo pediu a um policial para que nos levasse a conhecer os pontos mais atrativos, museus, praças e uma curto resumo histórico do povoado. Logo nos levou ao mirador do Índio, onde nos deram uma área no camping com um vista lindíssima de montanhas e o Valle del Cauca.
Saímos desse lindo lugar com nossos alforjes cheios de alimentos, o policial nos deu parte da sua cesta básica que recebe do estado e foi se despedir de nós na saída do povoado.
Aguadas, o povoado das névoas, e para mim o povoado da solidariedade.
Bem agora para chegar a Manizales, a terra do café, tínhamos que cruzar por Salamina e logo a Neira, mas os caminhos continuavam sendo complicados, quase tudo era sem asfalto e constantes subidas e descidas, como pedalar sobre um serrote gigante. Meu companheiro começou a ficar para trás cada vez mais, certo por vir com mais peso de equipamentos e também por estar um pouco mais gordinho que eu, mas assim fomos avançando devagar e com paciência. O povoado de Salamina se mostrou muito perto nos primeiros quilômetros do percurso, mas logo nos demos conta que não estava tão perto porque está em cima de um morro e para chegar até ele, tínhamos que rodar um profundo barranco que nos mentia porque creiamos que estávamos vendo aí muito perto, já chegando mas tínhamos que circular uma ou outra vez os morros e o precipício.
Em Salamina ninguém nos recebeu como em Aguadas, não tivemos outra senão pagar uma pousada barata e sair muito cedo até Neira. O caminho para lá foi um pouquinho mais amável com a gente, lembro que chegamos a uma parte muito bonita do caminho com muita neblina e nos aproximamos de uma cafeteria onde descansamos e tomamos café, mas a jovem dona do negócio, ao observar nossas bicicletas e ao escutar nossas aventuras, não quis cobrar e ainda mais nos presenteou biscoitos para o caminho. En Neira dormimos no posto dos bombeiros e consertamos a garupa da bicicleta do Enio na casa de um vezinho que tinha uma máquina de soldar.
No seguinte dia e a uns poucos quilômetros de chegar na cidade de Manizales, um senhor dono de um mega restaurante nos convidou a ficar dois dias no seu sítio, em uma cabana de luxo e com comida e um tour pelo seu cafezal. O tipo tinha um mini-zoológico, um lago artificial onde a gente pescava o peixe que queria comer e com shows ao vivo de danças tradicionais.
Era curioso que uns dias nos sentíamos como estrelas de rock e outros eramos dois simples loucos em bicicleta.
Quando chegamos a Manizales, Pilar nos escreveu para esperar ela na praça “Café Valdez”. Aí estivemos umas duas horas esperando–a, mais ainda trazíamos sorte, uma família que conhecemos e com a que conversamos um bom tempo nos presenteou a cada um de nós uma xícara gigante de café especial e biscoitos. Pilar chegou e nos abraçamos como se já fossemos velhos amigos. Logo ela nos levou para almoçar em um shopping center onde enquanto estávamos na fila para fazer nossos pedidos, uma senhora foi até mim para me tirar do lugar pensando que eu era um indigente. Claro, minha aparência não era “tão casual”, Pilar e Enio se incomodaram muito, foram me trazer de novo e comemos juntos.
Pilar vive numa gigantesca e lindíssima casa feita de bambu por ela mesma. É engenheira de bioconstrução e agora trabalha como professora na universidade de Manizales ensinando uma tese criada por ela mesma, chamada: a poética de caminhar. Imagina que nível mais alto de consciência tinha essa mulher.
Os dias que passamos na casa de Pilar foram fantásticos e foi aí que pela primeira vez Enio viu um morro nevado, o morro Nevado do Ruiz, que somente deixava se ver uns poucos minutos nas manhãs e logo se cobria de uma espessa neblina.
De Manizales saímos a Chinchiná acompanhados de Pilar, que regressou em ônibus até a sua casa. Aí estava a casa do ciclista de Chinchiná, uma das casas mais pequenas da América Latina, mas suficiente para um viajante cansado e agradecendo também seu anfitrião, um ser muito humano e carismático, ao qual conseguimos conhecer minutos antes de sairmos.
Chegamos a Casa do ciclista de Hectorino, em Ginebra, no Valle del Cauca muito perto da cidade de Cali. Aí conhecemos a família Happy, um casal italiano que viaja com suas duas filhinhas de bicicleta e com os quais, junto a Hectorino, fomos buscar ouro num rio onde encontramos um tesouro de momentos com os senhores que viviam em uma humilde casa a beira do rio. A mãe de Hectorino nos contava suas histórias de um modo tão agradável que não te cansava de lhe admirar e sorrir. Junto a ela, toda sua família era muito amável e tranquila. Uma tarde dessas chegou Paola, uma jovem que vinha viajando de bicicleta desde Bogotá, a capital de Colômbia, que vinha um pouco ruim do estômago porque tudo que comia vomitava. A noite seguinte chegou Luci, a jovem Argentina, depois os quatro saímos juntos da casa de Hectorino rumo ao fórum da bicicleta que este ano era celebrado na cidade branca de Popayán.
Era muito divertido vir quatro ciclistas carregados de malas pelas rodovias do Valle del Cauca. Paola tinha um contato de um amigo que estava disposto a nos dar alojamento em Jamundí, uma colônia fora da grande cidade de Cali. Chegamos, ele e sua esposa nos receberam muito contentes com um jantar muito saboroso. Daí arrancamos caminho a Piendamó onde Alcideades outro grande personagem, nos receberia na sua casa que está no meio de um paraíso de árvores frutais e junto com seu pai, que se levantava todos os dias as cinco da manhã para nos tostar e coar café da sua própria fazenda. Aí decidimos que nossa equipe tinha que ter um nome e foi entre Enio, Luci, Paola e eu que chegamos a um acordo de nos chamar CABES. Pelas iniciais de cada um de nosos países de origem, C de Colômbia, A de Argentina, B de Brasil e ES de El Salvador. E assim, como CABES nos apresentamos em um canal de televisão e no fórum nacional da bicicleta de Popayán.
Paola seguia vomitando o pouco que comia, mas nos disse que podia seguir com a gente um pouco mais. Já na cidade de Popayán, os organizadores nos designaram uns dormitórios nas instalações dos internos da Universidade de Nacional e estivemos como convidados internacionais, com algumas participações nas palestras. Ao final do evento, fui entrevistado por um canal digital, lhes contei minha experiência de viajar a outros países em bicicleta. Na noite, a festa foi protagonizada pela cultura afrodescendente de Cali, com ritmos alegres, música de tambores, danças eróticas e muito sensuais. Fiquei essa noite com Naty, uma das jovens da casa Dosquebradas, que havia chegado junto com outros amigos para fazer parte do fórum. Despedir-me  dela foi como arrancar um pedaço do coração, ainda escrevendo isso me arrepia a alma.
Alcancei os meninos em San Agustín, um povoado que está em um sítio arqueológico, cruzando os páramos e frailejones31. Daí nos despedimos de Paola que regressou pedindo que a levasse qualquer carro que quisesse ajudar, porque se deu conta que as náuseas que tinha era causada por uma bebê que estava gestando em seu ventre. A felicitamos e nos despedimos dela comovidos pelo feito.
Fomos daí e entramos ao “Trampolim da morte”, uma rota muito extrema de estrada estreita e de precipícios vertiginosos que já levaram a morte milhares de pessoas que transitaram em veículos fá faz muitos anos.
Os dois dias que levamos para cruzar foi de pura adrenalina e assombro, ao final, foi mais encantos naturais que medo de morrer. Chegamos a Sibundoy, onde um avó me ofereceu a cerimônia de “yagé”, mas seguindo as recomendações do meu guru não o fiz. Cabunga era um anfitrião nesse mágico e místico lugar e ainda ele não estava, a vizinha que nos foi abrir a casa, nos mostrou muito da vida sem nos dizer nada, bastou ver–la na sua cotidianidade de ser.
Daí Luci, Enio e eu saímos caminho a Pasto, onde na casa de Doris celebramos meu aniversário número 38 comendo pupusas, bolo de banana e brigadeiro, uma sobremesa brasileira. Saímos para a fronteira do Equador.
Aqui seria para mim outra etapa da viagem, onde definiria o que queria fazer e até onde queria chegar, seria cruzando essa fronteira que Chepe Ruiz (o salvadorenho que saiu de Garita Palmeira) começaria a viver uma das mais importantes etapas de uma viagem ciclodélica.
Estava apenas começando.

Atropelos
Ser atropelado numa rodovia é, sem dúvida, o pior que se pode passar com um cicloviageiro.
Lembro aqueles caminhões que passavam quase voando na estrada a caminho de Cali, alguns dos gigantes caminhões iam rebocando até quatro vagões carregados de cana-de-açúcar. Algumas vezes preferia me desviar um pouco para o mato e sair da estrada para deixar–los passar. Ao me lembrar disso me pergunto, de quantas ocasiões tivemos escapado do risco de morrer na rodovia?
Um amigo me contou que conseguiu por um espelhinho na sua bicicleta, mas de tantos tombos e descuidos que teve, o espelhinho terminou quebrando. Assim que, preferiu aprender a afinar seu ouvido. Contou-me que depois de uns poucos meses viajando na sua bicicleta, conseguiu diferenciar os sons sem virar para ver para trás.
Chegou ao ponto de adivinhar o que era aquilo que se aproximava desde longe, ouvia os sons e ele já sabia se era uma moto, um carro pequeno, um ônibus ou um caminhão. Descobriu que se pode chegara olhar com os ouvidos.
Estive buscando na internet sobre o tema e encontrei uma grande quantidade de desafortunados ciclistas que morreram nas rodovias atropelados por veículos motorizados. É muito trágico e é nas grandes cidades onde aumenta mais as possibilidades de se converter em uma vítima a mais.
Meu amigo me seguiu contanto que por sorte ou pelas graças do destino passou ileso por algumas das estreitas estradas da América Central e qual nunca se imaginou que seria atropelado na Colômbia, mas pelo amor.
Garantia também que devido à experiência, ser atropelado pelo amor pode chegar a ser o mesmo de trágico pelo efeito que deixa sequelas psicológicas.
– Não a escutei – me disse – nem a vi vindo. Ela passou como uma estrela cadente me rebocando na sua luz.
– Era fogo ardente, terminamos molhados na minha barraca, fazendo e fazendo uma e outra vez, excitado e abatido por seus beijos de língua de cobra.
Não consegui colocar resistência ante seu corpo desnudo e cintilante, a tatuagem do sistema solar que começa debaixo das suas axilas e que termina nas suas costelas, me fazia graça.
Seus gemidos de sirene me enfeitiçaram me deixando a deriva em um limbo sem rumo.
Me deixei arrastar quatro dias. Sim, quatro dias durou aquele atropelamento, me deixando todo dolorido mas com uma dor prazerosa e logo, o fez. Voltou, girou e afastando, se foi…
Tal qual um carro, que escapa deixando o pedestre ferido, sozinho, em plena rua debaixo da chuva.
– Nesse estado fiquei – disse – vendo o que ainda me restava inteiro para continuar.
Como consegui, me levantei, levantei a bicicleta e os restos do coração e fingi frente aos demais que não havia passado nada, que estava bem e continuei.
Levo já alguns meses tentando, mas ainda não me recuperei de tudo, sigo escutando sua voz, sua risada libidinosa, as queixas devido a suas pencas de orgasmos, tudo. E quando estou avançando em lhe esquecer, me chega na hora a imagem dos seus olhos negros, seus enormes seios e seu gordo traseiro.
Sim amigo.
Ser atropelado na rodovia é sem dúvida o pior que pode passar quando está viajando de bicicleta.
Ainda que fazendo–nos estúpidos, preferimos isso e não ser arrastado pelo amor, se namorando desse ser tão perfeito apesar dos seus defeitos, uma mulher que está certo que jamais voltará a ver.

Com ela
Fazer amor com ela pe se entregar no primeiro beijo debaixo de uma constelação de estrelas, é perder a vergonha no seu turbilhão de carícias rodeados dos olhares de estranhos e escutar o eco de ambos gemidos no imenso e frio salão desolado. Seus beijos têm sabor a vinho, cerveja, tabaco e paixão intensa.
Fazer amor com ela é encontrar um lugar não tão propício para desejos apenas, carente de amplitude, escasso de silêncio, ausente do bom aroma.
Pois é este o banheiro das mulheres de um bar, que obriga ir rápido, enquanto com um pé evita que as mulheres entrem para urinar.
Fazer amor com ela é fundir corpo e alma, sucumbindo em um furacão num solo exalar do seu hálito, é acordar no ventre de uma escultura num espaço para dois, com paredes de vitrais e num céu revelador.
Contemplar sua nudez enquanto ela sonha, beijar–la sem despertá–la é perder a noção do tempo e permanecer assim, encalhados, rodeados de escombros de um naufrágio intencional.
Fazer amor com ela é criar um tsunami numa piscina com o mover constante das suas cadeiras; é pois a grama, generosa e ampla cama; é pois o sol, quente e suficiente toalha e em gratidão a mãe terra... vão aí os olhares que sorriem esquecendo os nomes, juntos e nus chegar a ela. E não quero coisa alguma, que seja isso que minha alma sonha. Que nos encontre debaixo da lua, fazendo amor com ela.

Colômbia (em um verso)
Se chega em avião ou desce de um barco. Com certa discrepância arma tua bicicleta e te lança pelos seus caminhos e estradas que serpenteiam por suas praias, rios, desertos, páramos, cidades, montanhas. Por esses velhos povoados cheios de histórias modernas e coloniais.
Pode encontrar aqui um lar para cicloviageiros onde todos nos conhecemos, uma comuna num morro dançando break dance para os gringos, aldeias de névoas, chapéus, miragens e milagres, um tio que não te deixa ir até que tome outra cerveja, carros que param, mãos com garrafas de água, comida, dinheiro.
O vento cheira a café torrado e toda hora do dia, arepas32, água de panela33, chicha34, guarapo35 e pokeron36.
Uma maloca que treme quando comemora aniversário, o silêncio de uma selva, melancolias quando tocam os velhos vallenatos, a alegria das cumbias, o enraizamento de uma música popular.
Um povoado que tem o nome de Pensamos, Damos e Amamos.
Um fórum com palestras de esperanças, risadas, esculturas que dormem nas suas entranhas, as ruas com duras subidas e logo te consolam nas suas descidas.
Uma menina, uma mulher, um direito de lutar para tirar os carros das praças, o tempo não alcança, as emoções são muitas e como presente da vida peço um favor: que me deixem, ainda que seja nesse verso, dizer ao mundo que também sou um Colombiano.

Viaja
Viaja, não fique aí parado esperando a morte, viaja e avança até a vida. Desliga a televisão e liga tua própria história.
Viaja numa casa rodante com sua família ou pega a mochila, sai a rua e pede que te levem a qualquer lado.
Viaja numa moto, numa bicicleta, num patinete o vai caminhando, mas viaja.
Presenteia teus olhos com o milagre de contemplar a beleza das coisas e descubra a magia da natureza.
Conhecendo outros lugares provará outros sabores, escutará outros idiomas, outros sotaques.
Encontrará outras famílias, superará novos desafios, sobreviverá a outras tragédias.
Viaja e, no caminho, volta a amar, tem pessoas em todas partes que esperam te conhecer, assim que vai por eles e encontra–os.
Sai do seu país e logo sente saudade dele, gasta seu dinheiro em experiências e não acumule bens demais, que ao morrer o único que levará é o vivido.
Viaja, faz hoje que pode, não importa se não tem dinheiro, lembra que o meio mais forte é a força de vontade e, quando chegar a velhice e seja difícil mover–se, será o bastante com o lembrar cada dia as viagens que fez, os lugares que percorreu, os amigos que amou, o esbanjamento de alegria que teve.
Então voltará a se sentir jovem, voltará a sorrir e a se sentir amado. Voltará a ser feliz.



